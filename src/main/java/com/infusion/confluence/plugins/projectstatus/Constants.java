package com.infusion.confluence.plugins.projectstatus;

/**
 * @author Wojciech Kaczmarek
 */
public class Constants {

    private Constants() {
        // prevent from instantiating
    }

    // Epic
    public static final String EPIC_NAME_FIELD_NAME = "Epic Name";
    public static final String EPIC_LINK_FIELD_NAME = "Epic Link";
    public static final String ISSUE_TYPE_FIELD_NAME = "issuetype";
    public static final String NAME_FIELD_NAME = "name";
    public static final String EPIC_ISSUE_TYPE_NAME = "Epic";

    public static final String MILESTONES_FIELD_NAME = "Milestones";
    public static final String ROM_ESTIMATE_FIELD_NAME = "ROM Estimate";
    public static final String SPRINT_FIELD_NAME = "Sprint";
    public static final String LABELS_FIELD_NAME = "labels";
    public static final String STATUS_FIELD_NAME = "status";
    public static final String STATUS_CATEGORY_FIELD_NAME = "statusCategory";
    public static final String STATUS_CATEGORY_NAME_FIELD_NAME = "name";
    public static final String AGGREGATE_TIME_SPENT_FIELD_NAME = "aggregatetimespent";
    public static final String AGGREGATE_TIME_ESTIMATE_FIELD_NAME = "aggregatetimeestimate"; // remaining estimate
    public static final String AGGREGATE_PROGRESS_FIELD_NAME = "aggregateprogress";

    // Issue Types
    public static final String STORY_ISSUE_TYPE_NAME = "Story";
    public static final String BUG_ISSUE_TYPE = "Bug";

    // Errors
    public static final String ROM_ESTIMATE_FIELD_NOT_FOUND_MESSAGE = "Couldn't find 'ROM estimate' field";
}
