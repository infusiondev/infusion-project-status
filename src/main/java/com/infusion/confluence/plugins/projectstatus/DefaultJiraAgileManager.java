package com.infusion.confluence.plugins.projectstatus;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ResponseException;
import com.infusion.confluence.plugins.projectstatus.data.agile.RapidView;
import com.infusion.confluence.plugins.projectstatus.data.agile.SprintCalendarEntry;
import com.infusion.confluence.plugins.projectstatus.data.agile.SprintFromBoard;
import com.infusion.confluence.plugins.projectstatus.services.JiraConnectorService;
import com.infusion.confluence.plugins.projectstatus.util.ExceptionHelper;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.UriBuilder;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DefaultJiraAgileManager extends AbstractJiraClient implements JiraAgileManager {

    private static final Logger log = LoggerFactory.getLogger(DefaultJiraAgileManager.class);

    // XXX Private API
    private static final String REST_RAPIDVIEWS_LIST_URL = "/rest/greenhopper/1.0/rapidviews/list";
    private static final String REST_TEAMCALENDARS_SPRINT_LIST_URL = "/rest/greenhopper/1.0/integration/teamcalendars/sprint/list";
    private static final String REST_SPRINTQUERY_URL = "/rest/greenhopper/1.0/sprintquery/{rapidViewId}";

    private final ObjectMapper mapper = new ObjectMapper();

    public DefaultJiraAgileManager(JiraConnectorService jiraConnectorService) {
        super(jiraConnectorService);
    }

    @Override
    public List<RapidView> queryForRapidViews(String projectKey) throws CredentialsRequiredException, ResponseException {
        String url = UriBuilder.fromPath(REST_RAPIDVIEWS_LIST_URL).queryParam("projectKey", projectKey).build().toString();
        ApplicationLinkRequest request = createRequest(Request.MethodType.GET, url);
        String response = executeRequest(request);

        try {
            JsonNode root = mapper.readTree(response);
            JsonNode views = root.get("views");
            return mapper.readValue(views, new TypeReference<List<RapidView>>() {});
        } catch (Exception e) {
            log.error("Cannot parse response: {}", response, e);
            throw new ResponseException("There is a problem processing the response from JIRA: unrecognisable response: " + response, e);
        }
    }

    @Override
    public List<SprintCalendarEntry> queryForActiveSprints(String... projectKeys) throws CredentialsRequiredException, ResponseException {
        String jql = new JqlBuilder()
                .projectKeys(projectKeys)
                .and("Sprint not in closedSprints()")
                .buildRawJql();
        String url = UriBuilder.fromPath(REST_TEAMCALENDARS_SPRINT_LIST_URL).queryParam("jql", jql).build().toString();
        ApplicationLinkRequest request = createRequest(Request.MethodType.GET, url);
        String response = executeRequest(request);

        try {
            JsonNode root = mapper.readTree(response);
            JsonNode sprints = root.get("sprints");
            return mapper.readValue(sprints, new TypeReference<List<SprintCalendarEntry>>() {});
        } catch (Exception e) {
            log.error("Cannot parse response: {}", response, e);
            throw new ResponseException("There is a problem processing the response from JIRA: unrecognisable response: " + response, e);
        }
    }

    @Override
    public List<SprintFromBoard> queryForSprintsOnBoard(int rapidViewId, boolean includeFutureSprints) throws CredentialsRequiredException, ResponseException {
        String url = UriBuilder.fromPath(REST_SPRINTQUERY_URL).
                queryParam("includeFutureSprints", includeFutureSprints).build(rapidViewId).toString();
        ApplicationLinkRequest request = createRequest(Request.MethodType.GET, url);
        String response = executeRequest(request);

        try {
            JsonNode root = mapper.readTree(response);
            JsonNode sprints = root.get("sprints");
            return mapper.readValue(sprints, new TypeReference<List<SprintFromBoard>>() {
            });
        } catch (Exception e) {
            log.error("Cannot parse response: {}", response, e);
            throw new ResponseException("There is a problem processing the response from JIRA: unrecognisable response: " + response, e);
        }
    }

    @Override
    public Date queryForDueDate(String projectKey) throws CredentialsRequiredException, ResponseException {
        List<SprintFromBoard> sprintsFromBoard = queryForSprintsOnBoard(getMainBoard(projectKey).getId(), true);

        Map<Integer, SprintFromBoard> futureSprints = new HashMap<>();
        Map<Integer, SprintFromBoard> activeSprints = new HashMap<>();

        for (SprintFromBoard sprint : sprintsFromBoard) {
            if (Objects.equals(sprint.getState(), "FUTURE")) {
                futureSprints.put(sprint.getId(), sprint);
            } else if (Objects.equals(sprint.getState(), "ACTIVE")) {
                activeSprints.put(sprint.getId(), sprint);
            }
        }

        List<SprintCalendarEntry> calendarEntriesForActiveSprints =
                getCalendarEntries(activeSprints.values().stream().map(s -> s.getId()));

        // Sort active sprints by start date
        Collections.sort(calendarEntriesForActiveSprints, (o1, o2) -> o1.getStartDate().compareTo(o2.getStartDate()));

        SprintCalendarEntry activeSprint = calendarEntriesForActiveSprints.get(0);

        int sprintCount = activeSprints.size() + futureSprints.size();
        Duration sprintLength = ChronoUnit.WEEKS.getDuration().multipliedBy(ChronoUnit.WEEKS.between(activeSprint.getStartDate(), activeSprint.getEndDate()));

        ZonedDateTime dueDate = activeSprint.getStartDate().plus(sprintLength.multipliedBy(sprintCount));

        return Date.from(dueDate.toInstant());
    }

    @Override
    public RapidView getMainAgileBoard(String projectKey) {
        RapidView mainBoard;

        try {
            mainBoard = getMainBoard(projectKey);
        } catch (CredentialsRequiredException|ResponseException e) {
            throw ExceptionHelper.getJiraCommunicationException(e);
        }

        return mainBoard;
    }

    public List<SprintCalendarEntry> getCalendarEntries(Stream<Integer> sprintIds) throws CredentialsRequiredException, ResponseException {
        String jql = MessageFormat.format("Sprint in ({0})", sprintIds.map(id -> id.toString()).collect(Collectors.joining(",")));
        String url = UriBuilder.fromPath(REST_TEAMCALENDARS_SPRINT_LIST_URL).queryParam("jql", jql).build().toString();
        ApplicationLinkRequest request = createRequest(Request.MethodType.GET, url);
        String response = executeRequest(request);

        try {
            JsonNode root = mapper.readTree(response);
            JsonNode sprints = root.get("sprints");
            return mapper.readValue(sprints, new TypeReference<List<SprintCalendarEntry>>() {});
        } catch (Exception e) {
            log.error("Cannot parse response: {}", response, e);
            throw new ResponseException("There is a problem processing the response from JIRA: unrecognisable response: " + response, e);
        }
    }

    private RapidView getMainBoard(String projectKey) throws CredentialsRequiredException, ResponseException {
        List<RapidView> boards = queryForRapidViews(projectKey);

        if (boards.isEmpty()) {
            throw new IllegalArgumentException("Cannot find main board in empty list");
        }

        if (boards.size() == 1) {
            return boards.get(0);
        }

        // There should be a board called "{ProjectId} Main Board"

        for (RapidView board : boards) {
            if (board.getName() != null && board.getName().contains("Main Board")) {
                return board;
            }
        }

        // fallback to board with lowest id which is likely the one that was created first
        Collections.sort(boards, (o1, o2) -> Integer.compare(o1.getId(), o2.getId()));
        return boards.get(0);
    }
}
