package com.infusion.confluence.plugins.projectstatus;

import com.infusion.confluence.plugins.projectstatus.data.JiraField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DefaultJiraFieldsRepository implements JiraFieldsRepository {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultJiraFieldsRepository.class);

    private final JiraIssuesManager jiraIssuesManager;
    private Map<String, JiraField> cachedFields;

    public DefaultJiraFieldsRepository(JiraIssuesManager jiraIssuesManager) {
        this.jiraIssuesManager = jiraIssuesManager;
    }

    private Map<String, JiraField> fetchApplicationFields() {
        try {
            LOG.info("Querying for JiraFields...");
            List<JiraField> jiraFields = jiraIssuesManager.queryForFields();
            LOG.info("JiraFields obtained.");
            return jiraFields.stream().collect(Collectors.toMap(JiraField::getName, Function.identity()));
        } catch (Exception e) {
            LOG.error("Error occurred when querying for JiraFields.", e);
            throw new RuntimeException("Error occurred when querying for JiraFields.", e);
        }
    }

    @Override
    public JiraField get(final String fieldName) {
        return getCache().get(fieldName);
    }

    private Map<String, JiraField> getCache() {
        if (cachedFields == null) {
            cachedFields = fetchApplicationFields();
        }

        return cachedFields;
    }
}
