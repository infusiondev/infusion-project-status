package com.infusion.confluence.plugins.projectstatus;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.confluence.web.UrlBuilder;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.collect.ImmutableMap;
import com.google.gson.*;
import com.infusion.confluence.plugins.projectstatus.data.*;
import com.infusion.confluence.plugins.projectstatus.services.JiraConnectorService;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.infusion.confluence.plugins.projectstatus.util.Util.stream;

/**
 * @author Wojciech Kaczmarek
 */
public class DefaultJiraIssuesManager extends AbstractJiraClient implements JiraIssuesManager {

    private static final String REST_SEARCH_URL = "/rest/api/2/search";
    private static final String REST_ISSUE_TYPE_URL = "/rest/api/2/issuetype";
    private static final String REST_FIELD_URL = "/rest/api/2/field";
    private static final String REST_PROJECT_URL = "/rest/api/2/project";
    private static final String REST_STATUS_URL = "/rest/api/2/status";
    private static final String REST_STATUS_CATEGORY_URL = "/rest/api/2/statuscategory";
    private static final String REST_SUGGESTIONS_URL = "/rest/api/2/jql/autocompletedata/suggestions";

    public DefaultJiraIssuesManager(JiraConnectorService jiraConnectorService) {
        super(jiraConnectorService);
    }

    @Override
    public String executeJqlGetQuery(String jqlQuery) throws CredentialsRequiredException, ResponseException {
        String restUrl = REST_SEARCH_URL + "?" + jqlQuery;
        ApplicationLinkRequest request = createRequest(Request.MethodType.GET, restUrl);
        return executeRequest(request);
    }

    @Override
    public String executeJqlPostQuery(String jql) throws CredentialsRequiredException, ResponseException {
        ApplicationLinkRequest request = createRequest(Request.MethodType.POST, REST_SEARCH_URL);
        request.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON);
        request.setRequestBody(jql);
        return executeRequest(request);
    }

    @Override
    public List<JiraIssueType> queryForIssueTypes() throws CredentialsRequiredException, ResponseException {
        ApplicationLinkRequest request = createRequest(Request.MethodType.GET, REST_ISSUE_TYPE_URL);
        String response = executeRequest(request);
        return parseResponse(response, JiraIssueType.class);
    }

    @Override
    public List<JiraStatus> queryForStatuses() throws CredentialsRequiredException, ResponseException {
        ApplicationLinkRequest request = createRequest(Request.MethodType.GET, REST_STATUS_URL);
        String response = executeRequest(request);
        return parseResponse(response, JiraStatus.class);
    }

    @Override
    public List<JiraStatusCategory> queryForStatusCategories() throws CredentialsRequiredException, ResponseException {
        ApplicationLinkRequest request = createRequest(Request.MethodType.GET, REST_STATUS_CATEGORY_URL);
        String response = executeRequest(request);
        return parseResponse(response, JiraStatusCategory.class);
    }

    @Override
    public List<String> queryForLabels() throws CredentialsRequiredException, ResponseException {
        String url = new UrlBuilder(REST_SUGGESTIONS_URL).add("fieldName", "labels").toUrl();
        ApplicationLinkRequest request = createRequest(Request.MethodType.GET, url);
        String response = executeRequest(request);

        try {
            JsonElement parsedResponse = new JsonParser().parse(response);
            return stream(parsedResponse.getAsJsonObject().getAsJsonArray("results"))
                    .map(r -> r.getAsJsonObject().get("value").getAsString())
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new ResponseException("There is a problem processing the response from JIRA: unrecognisable response: " + response, e);
        }
    }

    @Override
    public List<JiraField> queryForFields() throws CredentialsRequiredException, ResponseException {
        ApplicationLinkRequest request = createRequest(Request.MethodType.GET, REST_FIELD_URL);
        String response = executeRequest(request);
        return parseResponse(response, JiraField.class);
    }

    public List<JiraIssue> queryForIssues(String jql, Map<String, String> extraParams, String... fields) throws CredentialsRequiredException, ResponseException {
        Map<String, Object> params = new HashMap<>(extraParams);
        params.putAll(ImmutableMap.of("jql", jql, "fields", fields));
        return queryForIssues(params);
    }

    @Override
    public List<JiraIssue> queryForIssues(String jql, String... fields) throws CredentialsRequiredException, ResponseException {
        return queryForIssues(jql, ImmutableMap.of("maxResults", "1000"), fields);
    }

    @Override
    public List<JiraIssue> queryForIssues(String requestJson) throws CredentialsRequiredException, ResponseException {
        String response = executeJqlPostQuery(requestJson);

        JsonElement parsedResponse = new JsonParser().parse(response);
        if (parsedResponse != null && parsedResponse.isJsonObject()) {
            JsonObject jsonObject = parsedResponse.getAsJsonObject();
            if (jsonObject.has("issues") && jsonObject.get("issues").isJsonArray()) {
                JsonArray jsonArray = jsonObject.getAsJsonArray("issues");
                if (jsonArray.size() > 0) {
                    return unpackToObjects(response, jsonArray, JiraIssue.class);
                }
            }
        }
        return Collections.emptyList();
    }

    @Override
    public List<JiraProject> queryForProjects() throws CredentialsRequiredException, ResponseException {
        ApplicationLinkRequest request = createRequest(Request.MethodType.GET, REST_PROJECT_URL);
        String response = executeRequest(request);
        return parseResponse(response, JiraProject.class);
    }

    private List<JiraIssue> queryForIssues(Map<String, Object> params) throws CredentialsRequiredException, ResponseException {
        Gson gson = new Gson();
        String requestJson = gson.toJson(params);
        return queryForIssues(requestJson);
    }
}
