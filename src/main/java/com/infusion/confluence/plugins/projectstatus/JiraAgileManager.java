package com.infusion.confluence.plugins.projectstatus;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.sal.api.net.ResponseException;
import com.infusion.confluence.plugins.projectstatus.data.agile.RapidView;
import com.infusion.confluence.plugins.projectstatus.data.agile.SprintCalendarEntry;
import com.infusion.confluence.plugins.projectstatus.data.agile.SprintFromBoard;

import java.util.Date;
import java.util.List;

/**
 * Provides access to Agile plugin data (Greenhopper API).
 */
public interface JiraAgileManager {

    List<RapidView> queryForRapidViews(String projectKey) throws CredentialsRequiredException, ResponseException;

    List<SprintCalendarEntry> queryForActiveSprints(String... projectKeys) throws CredentialsRequiredException, ResponseException;

    List<SprintFromBoard> queryForSprintsOnBoard(int rapidViewId, boolean includeFutureSprints) throws CredentialsRequiredException, ResponseException;

    Date queryForDueDate(String projectKey) throws CredentialsRequiredException, ResponseException;

    RapidView getMainAgileBoard(String projectKey);
}
