package com.infusion.confluence.plugins.projectstatus;

import com.infusion.confluence.plugins.projectstatus.data.JiraField;

/**
 * @author Wojciech Kaczmarek
 */
public interface JiraFieldsRepository {

    JiraField get(String fieldName);
}
