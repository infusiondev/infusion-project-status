package com.infusion.confluence.plugins.projectstatus;

import com.infusion.confluence.plugins.projectstatus.data.JiraIssue;
import com.infusion.confluence.plugins.projectstatus.data.JiraStatusCategory;
import com.infusion.confluence.plugins.projectstatus.util.Util;

import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.infusion.confluence.plugins.projectstatus.Constants.*;
import static org.springframework.util.StringUtils.containsWhitespace;
import static org.springframework.util.StringUtils.quote;

/**
 * @author Wojciech Kaczmarek
 */
public class JiraIssueUtils {

    private final JiraFieldsRepository fieldsRepository;

    public JiraIssueUtils(JiraFieldsRepository fieldsRepository) {
        this.fieldsRepository = fieldsRepository;
    }

    public boolean isInProgress(JiraIssue issue) {
        JiraStatusCategory.Status statusCategory = statusCategory(issue);
        return JiraStatusCategory.Status.IN_PROGRESS.equals(statusCategory);
    }

    public boolean isDone(JiraIssue issue) {
        JiraStatusCategory.Status statusCategory = statusCategory(issue);
        return JiraStatusCategory.Status.DONE.equals(statusCategory);
    }

    public boolean isToDo(JiraIssue issue) {
        JiraStatusCategory.Status statusCategory = statusCategory(issue);
        return JiraStatusCategory.Status.TO_DO.equals(statusCategory);
    }

    public JiraStatusCategory.Status statusCategory(JiraIssue issue) {
        String statusCategory = (String) getFieldValue(issue, STATUS_FIELD_NAME, STATUS_CATEGORY_FIELD_NAME, STATUS_CATEGORY_NAME_FIELD_NAME);
        return JiraStatusCategory.Status.fromName(statusCategory);
    }

    public String issueType(JiraIssue issue) {
        String statusCategory = (String) getFieldValue(issue, ISSUE_TYPE_FIELD_NAME, NAME_FIELD_NAME);
        return statusCategory;
    }

    public Object getFieldValue(final JiraIssue issue, final String... fieldPath) {
        return Util.getStringValueByPath(issue.getFields(), fieldPath);
    }

    public <T> T getFieldValueOrDefault(final JiraIssue issue, T defaultValue, final String... fieldPath) {
        @SuppressWarnings("unchecked")
        T result = (T) getFieldValue(issue, fieldPath);

        return result == null ? defaultValue : result;
    }

    public boolean inActiveSprint(JiraIssue issue) {
        Object sprintField = issue.getFields().get(fieldsRepository.get(SPRINT_FIELD_NAME).getId());

        if (sprintField == null) {
            return false;
        }

        @SuppressWarnings("unchecked")
        Iterable<String> sprints = (Iterable<String>) sprintField;

        for (String sprint: sprints) {
            String[] sprintPropertiesParts = sprint
                    .substring(sprint.indexOf('[') + 1, sprint.lastIndexOf(']'))
                    .split("[,]");
            Map<String, String> sprintProperties = Stream
                    .of(sprintPropertiesParts)
                    .map(kv -> kv.split("[=]"))
                    .collect(Collectors.toMap(s -> s[0], s -> s[1]));

            if (sprintProperties.get("state").equals("ACTIVE")) {
                return true;
            }
        }

        return false;
    }
}
