package com.infusion.confluence.plugins.projectstatus;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.sal.api.net.ResponseException;
import com.infusion.confluence.plugins.projectstatus.data.*;

import java.util.List;
import java.util.Map;

/**
 * @author Wojciech Kaczmarek
 */
public interface JiraIssuesManager {

    /**
     * Execute JQL query base on application link, the form of JQL should contain "jql" prefix
     * @param jqlQuery jql string, the form should be look like: "jql=type=epic&amp;startAt=1"
     * @return String with JSON format.
     * @throws CredentialsRequiredException
     * @throws ResponseException
     */
    String executeJqlGetQuery(String jqlQuery) throws CredentialsRequiredException, ResponseException;

    /**
     * Execute JQL query base on application link
     * @param jqlJsonQuery jql string in JSON
     * @return String with JSON format.
     * @throws CredentialsRequiredException
     * @throws ResponseException
     */
    String executeJqlPostQuery(String jqlJsonQuery) throws CredentialsRequiredException, ResponseException;

    List<JiraIssueType> queryForIssueTypes() throws CredentialsRequiredException, ResponseException;

    List<JiraStatus> queryForStatuses() throws CredentialsRequiredException, ResponseException;

    List<JiraStatusCategory> queryForStatusCategories() throws CredentialsRequiredException, ResponseException;

    List<JiraField> queryForFields() throws CredentialsRequiredException, ResponseException;

    List<JiraIssue> queryForIssues(String jql, Map<String, String> extraParams, String... fields) throws CredentialsRequiredException, ResponseException;

    List<JiraIssue> queryForIssues(String jql, String... fields) throws CredentialsRequiredException, ResponseException;

    List<JiraIssue> queryForIssues(String jql) throws CredentialsRequiredException, ResponseException;

    List<JiraProject> queryForProjects() throws CredentialsRequiredException, ResponseException;

    List<String> queryForLabels() throws CredentialsRequiredException, ResponseException;
}
