package com.infusion.confluence.plugins.projectstatus.aggregator;

import com.infusion.confluence.plugins.projectstatus.JiraIssueUtils;
import com.infusion.confluence.plugins.projectstatus.data.IssueGroupDetails;
import com.infusion.confluence.plugins.projectstatus.data.IssueStatusClassificationCount;
import com.infusion.confluence.plugins.projectstatus.data.JiraIssue;
import com.infusion.confluence.plugins.projectstatus.data.JiraWorkProgress;
import com.infusion.confluence.plugins.projectstatus.util.Util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class IssueGroupDetailsAggregator implements IssuesAggregator<Collection<IssueGroupDetails>> {
    private final IssuesAggregator<Double> romProgressIssuesAggregator;
    private final IssuesAggregator<JiraWorkProgress> workProgressIssuesAggregator;
    private final IssuesAggregator<IssueStatusClassificationCount> issueStatusClassificationCountAggregator;
    private final Function<JiraIssue, String> groupIdProvider;
    private final Function<JiraIssue, String> displayNameProvider;
    private final JiraIssueUtils issueUtils;

    public IssueGroupDetailsAggregator(
            IssuesAggregator<Double> romProgressIssuesAggregator,
            IssuesAggregator<JiraWorkProgress> workProgressIssuesAggregator,
            IssuesAggregator<IssueStatusClassificationCount> issueStatusClassificationCountAggregator,
            Function<JiraIssue, String> groupIdProvider,
            Function<JiraIssue, String> displayNameProvider,
            JiraIssueUtils issueUtils) {
        this.romProgressIssuesAggregator = romProgressIssuesAggregator;
        this.workProgressIssuesAggregator = workProgressIssuesAggregator;
        this.issueStatusClassificationCountAggregator = issueStatusClassificationCountAggregator;
        this.groupIdProvider = groupIdProvider;
        this.displayNameProvider = displayNameProvider;
        this.issueUtils = issueUtils;
    }

    @Override
    public Collection<IssueGroupDetails> aggregate(Iterable<JiraIssue> issues) {
        return Util.buildAggregate(this, issues);
    }

    @Override
    public IssuesAggregationBuilder<Collection<IssueGroupDetails>> getAggregationBuilder() {
        return new IssueGroupDetailsAggregationBuilder();
    }

    private class IssueGroupDetailsAggregationBuilder implements IssuesAggregationBuilder<Collection<IssueGroupDetails>> {

        private final Map<String, IssueGroupDetailsData> map = new HashMap<>();

        private boolean feedCalled;

        public IssueGroupDetailsAggregationBuilder() {
        }

        @Override
        public void feed(JiraIssue issue) {
            feedCalled = true;

            final String issueGroupIdentifier = groupIdProvider.apply(issue);
            final String issueGroupDisplayName = displayNameProvider.apply(issue);

            IssueGroupDetailsData issueGroupDetailsData = Util.ensureEntry(map, issueGroupIdentifier, () -> new IssueGroupDetailsData(issueGroupDisplayName, issueGroupIdentifier));

            issueGroupDetailsData.feed(issue);
        }

        @Override
        public Collection<IssueGroupDetails> build() {
            if (!feedCalled) {
                return null;
            }

            return convertToDto(map);
        }

        private Collection<IssueGroupDetails> convertToDto(Map<String, IssueGroupDetailsData> map) {
            return map.entrySet().stream().map(entry -> {
                IssueGroupDetailsData issueGroupDetailsData = entry.getValue();
                IssueGroupDetails details = new IssueGroupDetails();
                IssueStatusClassificationCount issueStatusClassificationActiveSprintCount =
                        issueGroupDetailsData.issueStatusClassificationActiveSprintBuilder.build();
                IssueStatusClassificationCount issueStatusClassificationOutsideActiveSprintCount =
                        issueGroupDetailsData.issueStatusClassificationOutsideActiveSprintBuilder.build();
                IssueStatusClassificationCount issueStatusClassificationCount =
                        issueStatusClassificationActiveSprintCount.add(issueStatusClassificationOutsideActiveSprintCount);

                details.setKey(issueGroupDetailsData.key);
                details.setDisplayName(issueGroupDetailsData.displayName);
                details.setIssueStatusClassificationCount(issueStatusClassificationCount);
                details.setIssueStatusClassificationActiveSprintCount(issueStatusClassificationActiveSprintCount);
                details.setIssueStatusClassificationOutsideActiveSprintCount(issueStatusClassificationOutsideActiveSprintCount);
                details.setRomProgress(issueGroupDetailsData.romProgressAggregationBuilder.build());
                details.setWorkProgress(issueGroupDetailsData.workProgressAggregationBuilder.build());
                return details;
            }).collect(Collectors.toList());
        }

        private class IssueGroupDetailsData {

            public final String displayName;
            public final String key;
            public final IssuesAggregationBuilder<Double> romProgressAggregationBuilder;
            public final IssuesAggregationBuilder<JiraWorkProgress> workProgressAggregationBuilder;
            public final IssuesAggregationBuilder<IssueStatusClassificationCount> issueStatusClassificationOutsideActiveSprintBuilder;
            public final IssuesAggregationBuilder<IssueStatusClassificationCount> issueStatusClassificationActiveSprintBuilder;

            public IssueGroupDetailsData(String displayName, String key) {
                this.displayName = displayName;
                this.key = key;
                romProgressAggregationBuilder = romProgressIssuesAggregator.getAggregationBuilder();
                workProgressAggregationBuilder = workProgressIssuesAggregator.getAggregationBuilder();
                issueStatusClassificationActiveSprintBuilder = issueStatusClassificationCountAggregator.getAggregationBuilder();
                issueStatusClassificationOutsideActiveSprintBuilder = issueStatusClassificationCountAggregator.getAggregationBuilder();
            }

            public void feed(JiraIssue issue) {
                romProgressAggregationBuilder.feed(issue);
                workProgressAggregationBuilder.feed(issue);
                if (issueUtils.inActiveSprint(issue)) {
                    issueStatusClassificationActiveSprintBuilder.feed(issue);
                } else {
                    issueStatusClassificationOutsideActiveSprintBuilder.feed(issue);
                }
            }
        }
    }
}
