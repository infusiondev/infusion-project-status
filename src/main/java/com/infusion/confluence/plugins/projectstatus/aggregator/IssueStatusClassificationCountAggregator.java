package com.infusion.confluence.plugins.projectstatus.aggregator;

import com.infusion.confluence.plugins.projectstatus.JiraIssueUtils;
import com.infusion.confluence.plugins.projectstatus.data.IssueStatusClassificationCount;
import com.infusion.confluence.plugins.projectstatus.data.JiraIssue;
import com.infusion.confluence.plugins.projectstatus.util.Util;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Calculates total number of tasks in each state.
 */
@Qualifier("issueStatusClassificationCountAggregator")
public class IssueStatusClassificationCountAggregator implements IssuesAggregator<IssueStatusClassificationCount> {
    private final JiraIssueUtils issueUtils;

    public IssueStatusClassificationCountAggregator(JiraIssueUtils issueUtils) {
        this.issueUtils = issueUtils;
    }

    @Override
    public IssueStatusClassificationCount aggregate(Iterable<JiraIssue> issues) {
        return Util.buildAggregate(this, issues);
    }

    @Override
    public IssuesAggregationBuilder<IssueStatusClassificationCount> getAggregationBuilder() {
        return new IssueStatusClassificationCountAggregationBuilder();
    }

    private class IssueStatusClassificationCountAggregationBuilder implements IssuesAggregationBuilder<IssueStatusClassificationCount> {
        private int done, inProgress, total;

        @Override
        public void feed(JiraIssue issue) {

            total++;

            switch(issueUtils.statusCategory(issue)) {
                case IN_PROGRESS:
                    inProgress++;
                    break;
                case DONE:
                    done++;
                    break;
            }
        }

        @Override
        public IssueStatusClassificationCount build() {
            return new IssueStatusClassificationCount(done, inProgress, total);
        }
    }
}
