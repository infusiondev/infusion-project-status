package com.infusion.confluence.plugins.projectstatus.aggregator;

import com.infusion.confluence.plugins.projectstatus.data.JiraIssue;

public interface IssuesAggregationBuilder<V> {
    void feed(JiraIssue issue);
    V build();
}
