package com.infusion.confluence.plugins.projectstatus.aggregator;

import com.infusion.confluence.plugins.projectstatus.data.JiraIssue;

/**
 * @author Wojciech Kaczmarek
 */
public interface IssuesAggregator<V> {

    V aggregate(Iterable<JiraIssue> issues);

    IssuesAggregationBuilder<V> getAggregationBuilder();
}
