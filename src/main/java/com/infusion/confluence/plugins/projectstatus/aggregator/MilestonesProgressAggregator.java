package com.infusion.confluence.plugins.projectstatus.aggregator;

import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.infusion.confluence.plugins.projectstatus.data.IssueStatusClassificationCount;
import com.infusion.confluence.plugins.projectstatus.data.JiraIssue;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

/**
 * Calculates Milestone summary progress.
 */
//@Qualifier("milestonesProgressAggregator")
public class MilestonesProgressAggregator implements IssuesAggregator<IssueStatusClassificationCount> {

    private final IssuesAggregator<IssueStatusClassificationCount> issueStatusClassificationCountAggregator;
    private final Function<JiraIssue, String> milestoneExtractor;

    public MilestonesProgressAggregator(
            /*@Qualifier("issueStatusClassificationCountAggregator")*/ IssuesAggregator<IssueStatusClassificationCount> issueStatusClassificationCountAggregator,
            Function<JiraIssue, String> milestoneExtractor
    ) {
        this.issueStatusClassificationCountAggregator = issueStatusClassificationCountAggregator;
        this.milestoneExtractor = milestoneExtractor;
    }

    @Override
    public IssueStatusClassificationCount aggregate(Iterable<JiraIssue> issuesWithMilestones) {

        Multimap<String, JiraIssue> issuesByMilestones = Multimaps.index(issuesWithMilestones, milestoneExtractor::apply);

        int done = 0;
        int inProgress = 0;
        int total = 0;

        for (Map.Entry<String, Collection<JiraIssue>> entry : issuesByMilestones.asMap().entrySet()) {
            String milestone = entry.getKey();
            Collection<JiraIssue> milestoneIssues = entry.getValue();

            IssueStatusClassificationCount milestoneStatus = issueStatusClassificationCountAggregator.aggregate(milestoneIssues);

            total++;
            if (milestoneStatus.getIssuesInProgressCount() > 0) {
                inProgress++;
            } else if (milestoneStatus.getIssuesDoneCount() == milestoneStatus.getTotalCount()) {
                done++;
            }
        }

        return new IssueStatusClassificationCount(done, inProgress, total);
    }

    @Override
    public IssuesAggregationBuilder<IssueStatusClassificationCount> getAggregationBuilder() {
        throw new UnsupportedOperationException();
    }
}
