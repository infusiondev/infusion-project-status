package com.infusion.confluence.plugins.projectstatus.aggregator;

import com.infusion.confluence.plugins.projectstatus.JiraFieldsRepository;
import com.infusion.confluence.plugins.projectstatus.JiraIssueUtils;
import com.infusion.confluence.plugins.projectstatus.data.JiraField;
import com.infusion.confluence.plugins.projectstatus.data.JiraIssue;
import com.infusion.confluence.plugins.projectstatus.util.Util;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.infusion.confluence.plugins.projectstatus.Constants.*;

/**
 * Calculates sum of ROM estimates of all completed tasks.
 *
 * @author Wojciech Kaczmarek
 */
@Qualifier("romProgressIssuesAggregator")
public class RomProgressIssuesAggregator implements IssuesAggregator<Double> {

    private final JiraFieldsRepository fieldsRepository;
    private final JiraIssueUtils issueUtils;

    public RomProgressIssuesAggregator(JiraFieldsRepository fieldsRepository, JiraIssueUtils issueUtils) {
        this.fieldsRepository = fieldsRepository;
        this.issueUtils = issueUtils;
    }

    @Override
    public Double aggregate(Iterable<JiraIssue> issues) {
        return Util.buildAggregate(this, issues);
    }

    @Override
    public IssuesAggregationBuilder<Double> getAggregationBuilder() {
        return new RomProgressIssuesAggregationBuilder();
    }

    private class RomProgressIssuesAggregationBuilder implements IssuesAggregationBuilder<Double> {
        private final JiraField romEstimateField;
        private final JiraField sprintField;

        private double progress;
        private double total;
        private boolean feedCalled;

        public RomProgressIssuesAggregationBuilder() {
            romEstimateField = fieldsRepository.get(ROM_ESTIMATE_FIELD_NAME);
            sprintField = fieldsRepository.get(SPRINT_FIELD_NAME);

            if (romEstimateField == null) {
                throw new RuntimeException(ROM_ESTIMATE_FIELD_NOT_FOUND_MESSAGE);
            }
        }

        @Override
        public void feed(JiraIssue issue) {
            feedCalled = true;
            @SuppressWarnings("unchecked")
            Map<String, Integer> aggregateProgress = (Map<String, Integer>)issueUtils.getFieldValue(issue, AGGREGATE_PROGRESS_FIELD_NAME);

            if (issueUtils.isDone(issue)) {
                progress += Util.secondsToHours(aggregateProgress.get("progress"));
                total += Util.secondsToHours(aggregateProgress.get("progress"));
            } else if (issueUtils.inActiveSprint(issue) || issueUtils.isInProgress(issue)) {
                progress += Util.secondsToHours(aggregateProgress.get("progress"));
                total += Util.secondsToHours(aggregateProgress.get("total"));
            } else if (issueUtils.isToDo(issue)) {
                Double romEstimate = issueUtils.getFieldValueOrDefault(issue, 0d, romEstimateField.getId());
                total += romEstimate;
            }
        }

        @Override
        public Double build() {
            if (!feedCalled) {
                return 1d;
            }

            return total == 0 ? 0 : progress / total;
        }
    }
}
