package com.infusion.confluence.plugins.projectstatus.aggregator;

import com.infusion.confluence.plugins.projectstatus.JiraFieldsRepository;
import com.infusion.confluence.plugins.projectstatus.JiraIssueUtils;
import com.infusion.confluence.plugins.projectstatus.data.JiraField;
import com.infusion.confluence.plugins.projectstatus.data.JiraIssue;
import com.infusion.confluence.plugins.projectstatus.data.JiraStatusCategory;
import com.infusion.confluence.plugins.projectstatus.data.JiraWorkProgress;
import com.infusion.confluence.plugins.projectstatus.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Map;

import static com.infusion.confluence.plugins.projectstatus.Constants.*;

/**
 * Calculates time spent on completed issues (stories, bugs) and total time as sum of time spent and ROM estimates of unfinished issues.
 *
 * FIXME stories have aggregateprogress field which seems to contain similar info !!!
 *
 * @author Wojciech Kaczmarek
 */
@Qualifier("workProgressIssuesAggregator")
public class WorkProgressIssuesAggregator implements IssuesAggregator<JiraWorkProgress> {

    private static final Logger LOG = LoggerFactory.getLogger(WorkProgressIssuesAggregator.class);

    private final JiraFieldsRepository fieldsRepository;
    private final JiraIssueUtils issueUtils;

    public WorkProgressIssuesAggregator(JiraFieldsRepository fieldsRepository, JiraIssueUtils issueUtils) {
        this.fieldsRepository = fieldsRepository;
        this.issueUtils = issueUtils;
    }

    @Override
    public JiraWorkProgress aggregate(Iterable<JiraIssue> issues) {
        return Util.buildAggregate(this, issues);
    }

    @Override
    public IssuesAggregationBuilder<JiraWorkProgress> getAggregationBuilder() {
        return new WorkProgressIssuesAggregationBuilder();
    }

    private class WorkProgressIssuesAggregationBuilder implements IssuesAggregationBuilder<JiraWorkProgress> {
        private final JiraField romEstimateField;

        private double total;
        private double logged;
        private double loggedDone;
        private double loggedInProgress;

        public WorkProgressIssuesAggregationBuilder() {
            romEstimateField = fieldsRepository.get(ROM_ESTIMATE_FIELD_NAME);

            if (romEstimateField == null) {
                throw new RuntimeException(ROM_ESTIMATE_FIELD_NOT_FOUND_MESSAGE);
            }
        }

        @Override
        public void feed(JiraIssue issue) {

            double romEstimate = issueUtils.getFieldValueOrDefault(issue, 0d, romEstimateField.getId());
            @SuppressWarnings("unchecked")
            Map<String, Integer> aggregateProgress = (Map<String, Integer>)issueUtils.getFieldValue(issue, AGGREGATE_PROGRESS_FIELD_NAME);

            JiraStatusCategory.Status statusCategory = issueUtils.statusCategory(issue);

            // maybe it's better to ignore status and count time when present, otherwise ROM ?

            switch (statusCategory) {
                case DONE:
                    loggedDone += Util.secondsToHours(aggregateProgress.get("progress"));
                    logged += Util.secondsToHours(aggregateProgress.get("progress"));
                    total += Util.secondsToHours(aggregateProgress.get("progress"));
                    break;
                case IN_PROGRESS:
                    loggedInProgress += Util.secondsToHours(aggregateProgress.get("progress"));
                    logged += Util.secondsToHours(aggregateProgress.get("progress"));
                    total += Util.secondsToHours(aggregateProgress.get("total"));
                    break;
                default:
                    total += romEstimate;
                    break;
            }
        }

        @Override
        public JiraWorkProgress build() {
            return new JiraWorkProgress(logged, total, loggedDone, loggedInProgress);
        }
    }
}
