package com.infusion.confluence.plugins.projectstatus.data;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;
import java.util.Comparator;

/**
 * @author Wojciech Kaczmarek
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class IssueGroupDetails implements Serializable {

    public static transient final Comparator<IssueGroupDetails> COMPARATOR = new NaturalOrder();

    @JsonProperty
    private String displayName;

    @JsonProperty
    private double romProgress;

    @JsonProperty
    private JiraWorkProgress workProgress;

    @JsonProperty
    private IssueStatusClassificationCount issueStatusClassificationCount;
    private IssueStatusClassificationCount issueStatusClassificationActiveSprintCount;
    private IssueStatusClassificationCount issueStatusClassificationOutsideActiveSprintCount;
    private String key;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public double getRomProgress() {
        return romProgress;
    }

    public void setRomProgress(double romProgress) {
        this.romProgress = romProgress;
    }

    public JiraWorkProgress getWorkProgress() {
        return workProgress;
    }

    public void setWorkProgress(JiraWorkProgress workProgress) {
        this.workProgress = workProgress;
    }

    public IssueStatusClassificationCount getIssueStatusClassificationCount() {
        return issueStatusClassificationCount;
    }

    public void setIssueStatusClassificationCount(IssueStatusClassificationCount issueStatusClassificationCount) {
        this.issueStatusClassificationCount = issueStatusClassificationCount;
    }

    public void setIssueStatusClassificationActiveSprintCount(IssueStatusClassificationCount issueStatusClassificationActiveSprintCount) {
        this.issueStatusClassificationActiveSprintCount = issueStatusClassificationActiveSprintCount;
    }

    public IssueStatusClassificationCount getIssueStatusClassificationActiveSprintCount() {
        return issueStatusClassificationActiveSprintCount;
    }

    public void setIssueStatusClassificationOutsideActiveSprintCount(IssueStatusClassificationCount issueStatusClassificationOutsideActiveSprintCount) {
        this.issueStatusClassificationOutsideActiveSprintCount = issueStatusClassificationOutsideActiveSprintCount;
    }

    public IssueStatusClassificationCount getIssueStatusClassificationOutsideActiveSprintCount() {
        return issueStatusClassificationOutsideActiveSprintCount;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return this.key;
    }

    static class NaturalOrder implements Comparator<IssueGroupDetails> {

        @Override
        public int compare(IssueGroupDetails o1, IssueGroupDetails o2) {
            return o1.getDisplayName().compareToIgnoreCase(o2.getDisplayName());
        }
    }

}
