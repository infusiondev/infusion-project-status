package com.infusion.confluence.plugins.projectstatus.data;

import org.codehaus.jackson.annotate.JsonProperty;

public class IssueStatusClassificationCount {
    @JsonProperty
    private int totalCount;

    @JsonProperty
    private int issuesInProgressCount;

    @JsonProperty
    private int issuesDoneCount;

    public IssueStatusClassificationCount(int issuesDoneCount, int issuesInProgressCount, int totalCount) {
        this.issuesDoneCount = issuesDoneCount;
        this.issuesInProgressCount = issuesInProgressCount;
        this.totalCount = totalCount;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int total) {
        this.totalCount = total;
    }

    public int getIssuesInProgressCount() {
        return issuesInProgressCount;
    }

    public void setIssuesInProgressCount(int issuesInProgressCount) {
        this.issuesInProgressCount = issuesInProgressCount;
    }

    public int getIssuesDoneCount() {
        return issuesDoneCount;
    }

    public void setIssuesDoneCount(int issuesDoneCount) {
        this.issuesDoneCount = issuesDoneCount;
    }

    public IssueStatusClassificationCount add(IssueStatusClassificationCount other) {
        return new IssueStatusClassificationCount(
                getIssuesDoneCount() + other.getIssuesDoneCount(),
                getIssuesInProgressCount() + other.getIssuesInProgressCount(),
                getTotalCount() + other.getTotalCount());
    }
}
