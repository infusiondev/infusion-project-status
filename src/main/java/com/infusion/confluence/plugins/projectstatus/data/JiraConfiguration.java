package com.infusion.confluence.plugins.projectstatus.data;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;

/**
 * @author Wojciech Kaczmarek
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JiraConfiguration implements Serializable {

    @JsonProperty
    private boolean timeTrackingEnabled;

    @JsonProperty
    private JiraTimeTrackingConfiguration timeTrackingConfiguration;

    public boolean isTimeTrackingEnabled() {
        return timeTrackingEnabled;
    }

    public void setTimeTrackingEnabled(boolean timeTrackingEnabled) {
        this.timeTrackingEnabled = timeTrackingEnabled;
    }

    public JiraTimeTrackingConfiguration getTimeTrackingConfiguration() {
        return timeTrackingConfiguration;
    }

    public void setTimeTrackingConfiguration(JiraTimeTrackingConfiguration timeTrackingConfiguration) {
        this.timeTrackingConfiguration = timeTrackingConfiguration;
    }
}
