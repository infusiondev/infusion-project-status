package com.infusion.confluence.plugins.projectstatus.data;

import com.google.common.collect.ImmutableSortedSet;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;

import static com.infusion.confluence.plugins.projectstatus.Constants.LABELS_FIELD_NAME;

/**
 * @author Wojciech Kaczmarek
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JiraIssue implements Serializable {

    @JsonProperty
    private String id;

    @JsonProperty
    private String key;

    @JsonProperty
    private Map<String, Object> fields;

    // Properties ------------------------------------------------------------------------------------------------------

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Map<String, Object> getFields() {
        return fields;
    }

    public void setFields(Map<String, Object> fields) {
        this.fields = fields;
    }

    @JsonIgnore
    public Set<String> getLabels() {
        return ImmutableSortedSet.copyOf(getFieldValues(LABELS_FIELD_NAME, String.class::cast));
    }

    // Helpers ---------------------------------------------------------------------------------------------------------

    public String getFieldValue(JiraField field) {
        return getFieldValue(field, String.class::cast);
    }

    public <T> T getFieldValue(JiraField field, Function<Object, T> converter) {
        return getFieldValue(field.getId(), converter);
    }

    private <T> T getFieldValue(String fieldName, Function<Object, T> converter) {
        Object fieldValue = fields.get(fieldName);

        if (fieldValue instanceof Collection) {
            Collection collection = (Collection) fieldValue;
            if (!collection.isEmpty()) {
                return converter.apply(collection.iterator().next());
            }
        } else if (fieldValue != null) {
            return converter.apply(fieldValue);
        }

        return null;
    }

    private <E> List<E> getFieldValues(String fieldName, Function<Object, E> converter) {
        Object fieldValue = fields.get(fieldName);

        List<E> result = new ArrayList<>();

        if (fieldValue instanceof Collection) {
            Collection<?> collection = (Collection<?>) fieldValue;
            for (Object item : collection) {
                result.add(converter.apply(item));
            }
        } else if (fieldValue != null) {
            result.add(converter.apply(fieldValue));
        }

        return result;
    }
}
