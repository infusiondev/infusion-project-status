package com.infusion.confluence.plugins.projectstatus.data;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;
import java.util.EnumSet;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JiraStatusCategory implements Serializable {

    @JsonProperty
    private String id;

    @JsonProperty
    private String self;

    @JsonProperty
    private String key;

    @JsonProperty
    private String colorName;

    @JsonProperty
    private String name;

    public enum Status {

        NO_CATEGORY("No Category"),
        TO_DO("To Do"),
        IN_PROGRESS("In Progress"),
        DONE("Done");

        private final String name;

        Status(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static Status fromName(String name) {
            for (Status s : EnumSet.allOf(Status.class)) {
                if (name.equalsIgnoreCase(s.getName())) {
                    return s;
                }
            }
            return null;
        }

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }
}
