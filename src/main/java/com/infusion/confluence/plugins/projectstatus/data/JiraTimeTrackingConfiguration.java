package com.infusion.confluence.plugins.projectstatus.data;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;

/**
 * @author Wojciech Kaczmarek
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JiraTimeTrackingConfiguration implements Serializable {

    @JsonProperty
    private int workingHoursPerDay;

    @JsonProperty
    private int workingDaysPerWeek;

    @JsonProperty
    private String defaultUnit;

    public int getWorkingHoursPerDay() {
        return workingHoursPerDay;
    }

    public void setWorkingHoursPerDay(int workingHoursPerDay) {
        this.workingHoursPerDay = workingHoursPerDay;
    }

    public int getWorkingDaysPerWeek() {
        return workingDaysPerWeek;
    }

    public void setWorkingDaysPerWeek(int workingDaysPerWeek) {
        this.workingDaysPerWeek = workingDaysPerWeek;
    }

    public String getDefaultUnit() {
        return defaultUnit;
    }

    public void setDefaultUnit(String defaultUnit) {
        this.defaultUnit = defaultUnit;
    }
}
