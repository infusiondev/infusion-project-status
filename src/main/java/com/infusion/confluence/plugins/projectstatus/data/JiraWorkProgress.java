package com.infusion.confluence.plugins.projectstatus.data;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;

/**
 * @author Wojciech Kaczmarek
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JiraWorkProgress implements Serializable {

    /**
     * Time spent on completed issues.
     */
    @JsonProperty
    private double hoursWorked;

    /**
     * Time spent on completed issues plus ROM estimates on unfinished issues.
     */
    @JsonProperty
    private double hoursTotal;

    /**
     * Time spent on completed issues.
     */
    @JsonProperty
    private double hoursDone;

    /**
     * Time spent + remaining estimates of in-progress issues.
     */
    @JsonProperty
    private double hoursInProgress;

    public JiraWorkProgress() {}

    public JiraWorkProgress(double hoursWorked, double hoursTotal, double hoursDone, double hoursInProgress) {
        this.hoursWorked = hoursWorked;
        this.hoursTotal = hoursTotal;
        this.hoursDone = hoursDone;
        this.hoursInProgress = hoursInProgress;
    }

    public double getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(double hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public double getHoursTotal() {
        return hoursTotal;
    }

    public void setHoursTotal(double hoursTotal) {
        this.hoursTotal = hoursTotal;
    }

    public double getHoursDone() {
        return hoursDone;
    }

    public void setHoursDone(double hoursDone) {
        this.hoursDone = hoursDone;
    }

    public double getHoursInProgress() {
        return hoursInProgress;
    }

    public void setHoursInProgress(double hoursInProgress) {
        this.hoursInProgress = hoursInProgress;
    }
}
