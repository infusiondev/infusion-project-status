package com.infusion.confluence.plugins.projectstatus.data;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MilestoneStats implements Serializable {

    @JsonProperty
    private String name;

    @JsonProperty
    private int totalCount;

    @JsonProperty
    private int doneCount;

    @JsonProperty
    private int inProgressCount;

    @JsonProperty
    private double romProgressEstimate;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getDoneCount() {
        return doneCount;
    }

    public void setDoneCount(int doneCount) {
        this.doneCount = doneCount;
    }

    public int getInProgressCount() {
        return inProgressCount;
    }

    public void setInProgressCount(int inProgressCount) {
        this.inProgressCount = inProgressCount;
    }

    public double getRomProgressEstimate() {
        return romProgressEstimate;
    }

    public void setRomProgressEstimate(double romProgressEstimate) {
        this.romProgressEstimate = romProgressEstimate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
