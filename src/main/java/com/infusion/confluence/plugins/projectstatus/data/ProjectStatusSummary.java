package com.infusion.confluence.plugins.projectstatus.data;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectStatusSummary {

    @JsonProperty
    private Double romProgress;

    @JsonProperty
    private Date dueDate;

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Double getRomProgress() {
        return romProgress;
    }

    public void setRomProgress(Double romProgress) {
        this.romProgress = romProgress;
    }
}
