package com.infusion.confluence.plugins.projectstatus.data;

import com.infusion.confluence.plugins.projectstatus.JiraIssueUtils;
import com.infusion.confluence.plugins.projectstatus.JqlBuilder;

import java.util.Set;

public class QueryParameters {

    private Set<String> excludedStatuses;
    private Set<String> includedLabels;

    public static final QueryParameters DEFAULT = new QueryParameters();

    public static QueryParameters defaultValue(QueryParameters parameters) {
        if (parameters == null)
            return DEFAULT;
        return parameters;
    }

    /**
     * List of status names that will be used to exclude stories and bugs from metrics.
     *
     * @return
     */
    public Set<String> getExcludedStatuses() {
        return excludedStatuses;
    }

    public void setExcludedStatuses(Set<String> excludedStatuses) {
        this.excludedStatuses = excludedStatuses;
    }

    public Set<String> getIncludedLabels() {
        return includedLabels;
    }

    public void setIncludedLabels(Set<String> includedLabels) {
        this.includedLabels = includedLabels;
    }


    // Filter generators -----------------------------------------------------------------------------------------------

    public void addFilters(JqlBuilder jqlBuilder) {
        addExcludedStatusesFilter(jqlBuilder);
        addIncludedLabelsFilter(jqlBuilder);
    }

    public void addIncludedLabelsFilter(JqlBuilder jqlBuilder) {
        if (getIncludedLabels() != null) {
            jqlBuilder.labels(getIncludedLabels().toArray(new String[0]));
        }
    }

    public void addExcludedStatusesFilter(JqlBuilder jqlBuilder) {
        if (getExcludedStatuses() != null) {
            jqlBuilder.not().statuses(getExcludedStatuses().toArray(new String[0]));
        }
    }
}
