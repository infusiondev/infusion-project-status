package com.infusion.confluence.plugins.projectstatus.data.agile;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RapidView {
    private int id;
    private String name;
    private boolean sprintSupportEnabled;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSprintSupportEnabled() {
        return sprintSupportEnabled;
    }

    public void setSprintSupportEnabled(boolean sprintSupportEnabled) {
        this.sprintSupportEnabled = sprintSupportEnabled;
    }
}
