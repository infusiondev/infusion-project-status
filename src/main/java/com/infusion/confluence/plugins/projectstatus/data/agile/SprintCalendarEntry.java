package com.infusion.confluence.plugins.projectstatus.data.agile;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SprintCalendarEntry {
    private int id;
    private String name;
    private String start;
    private String end;
    private boolean closed;
    private List<ProjectRef> projects;

    private ZonedDateTime parseDate(String date) {
        // Example date: 02062015170000
        // FIXME this time is in local timezone, sent in jodaTimeZoneId field of parent object
        ZoneId timeZone = ZoneId.systemDefault();
        // for now we assume same timezone in jira and confluence, besides we only need date
        // there are two digits after minutes but they are not seconds (can be >60)!
        DateTimeFormatter format = DateTimeFormatter.ofPattern("ddMMyyyyHHmm");
        return LocalDateTime.parse(date.substring(0, 12), format).atZone(timeZone);
    }

    @JsonIgnore
    public ZonedDateTime getStartDate() {
        return parseDate(getStart());
    }

    @JsonIgnore
    public ZonedDateTime getEndDate() {
        return parseDate(getEnd());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public List<ProjectRef> getProjects() {
        return projects;
    }

    public void setProjects(List<ProjectRef> projects) {
        this.projects = projects;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ProjectRef {
        private String key;
        private String name;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
