package com.infusion.confluence.plugins.projectstatus.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;

import java.util.Map;

public class GenericMacro implements Macro {

    private final MacroRenderer macroRenderer;

    public GenericMacro(MacroRenderer macroRenderer) {
        this.macroRenderer = macroRenderer;
    }

    @Override
    public String execute(Map<String, String> macroParameters, String bodyContent, ConversionContext conversionContext) {
        return macroRenderer.render(macroParameters, conversionContext);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
