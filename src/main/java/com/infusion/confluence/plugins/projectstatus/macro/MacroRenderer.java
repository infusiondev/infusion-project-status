package com.infusion.confluence.plugins.projectstatus.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.HtmlToXmlConverter;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.google.common.collect.ImmutableMap;
import com.infusion.confluence.plugins.projectstatus.util.PluginDescriptor;
import com.infusion.confluence.plugins.projectstatus.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.Map;

public class MacroRenderer {
    private static final Logger log = LoggerFactory.getLogger(MacroRenderer.class);

    private final HtmlToXmlConverter htmlToXmlConverter;
    private final PluginDescriptor pluginDescriptor;

    private final static String PROJECT_SUMMARY_MACRO_NAME = "project-summary";
    private final static String EPICS_SUMMARY_MACRO_NAME = "epics-summary";
    private final static String EPICS_DETAILS_MACRO_NAME = "epics-details";
    private final static String MILESTONES_SUMMARY_MACRO_NAME = "milestones-summary";
    private final static String MILESTONES_DETAILS_MACRO_NAME = "milestones-details";

    private final Map<String, ViewModelProvider> viewModelProviderMap;

    public MacroRenderer(HtmlToXmlConverter htmlToXmlConverter,
                         PluginDescriptor pluginDescriptor,
                         ViewModelFactory viewModelFactory,
                         RemoteDataProvider remoteDataProvider) {

        this.htmlToXmlConverter = htmlToXmlConverter;
        this.pluginDescriptor = pluginDescriptor;

        this.viewModelProviderMap = ImmutableMap.of(
            PROJECT_SUMMARY_MACRO_NAME, createViewModelProvider(
                        remoteDataProvider::getProjectSummaryRemoteData,
                        viewModelFactory::createProjectSummaryViewModel),
            EPICS_SUMMARY_MACRO_NAME, createViewModelProvider(
                        remoteDataProvider::getEpicsSummaryRemoteData,
                        viewModelFactory::createEpicsSummaryViewModel),
            EPICS_DETAILS_MACRO_NAME, createViewModelProvider(
                        remoteDataProvider::getEpicsDetailsRemoteData,
                        viewModelFactory::createEpicsDetailsViewModel),
            MILESTONES_SUMMARY_MACRO_NAME, createViewModelProvider(
                        remoteDataProvider::getMilestonesSummaryRemoteData,
                        viewModelFactory::createMilestonesSummaryViewModel),
            MILESTONES_DETAILS_MACRO_NAME, createViewModelProvider(
                        remoteDataProvider::getMilestonesDetailsRemoteData,
                        viewModelFactory::createMilestonesDetailsViewModel)
        );
    }

    public String render(Map<String, String> macroParameters, ConversionContext conversionContext) {
        String macroName = conversionContext.getProperty("macroName").toString();
        String templatePath = MessageFormat.format("{0}/template.vm", macroName);

        log.debug("[rendering {}] parameters: {}", macroName, macroParameters);

        pluginDescriptor.applyDefaultParameterValues(
                macroName,
                macroParameters);

        ViewModelProvider viewModelProvider = this.viewModelProviderMap.get(macroName);
        Map<String, Object> viewModel = viewModelProvider.getViewModel(macroParameters);
        Map<String, Object> velocityContext = MacroUtils.defaultVelocityContext();
        @SuppressWarnings("unchecked")
        Map<String, Object> velocityViewModel = Util.combineMaps(viewModel, velocityContext);

        log.debug("[rendering {}] view model: {}", macroName, viewModel);

        // ensure correct XHTML
        String html = htmlToXmlConverter.convert(VelocityUtils.getRenderedTemplate(templatePath, velocityViewModel));
        log.debug("[rendering {}] HTML output: {}", macroName, html);

        return html;
    }

    private static <TRemoteData> ViewModelProvider createViewModelProvider(
            RemoteDataGetterFunctionalInterface<TRemoteData> remoteDataProvider,
            ViewModelFactoryFunctionalInterface<TRemoteData> viewModelFactory) {
        return macroParameters -> {
            TRemoteData remoteData = remoteDataProvider.getData(macroParameters);
            return viewModelFactory.getViewModel(remoteData, macroParameters);
        };
    }

    private interface ViewModelProvider {
        Map<String, Object> getViewModel(Map<String, String> macroParameters);
    }

    private interface RemoteDataGetterFunctionalInterface<TRemoteData> {
        TRemoteData getData(Map<String, String> macroParameters);
    }

    private interface ViewModelFactoryFunctionalInterface<TRemoteData> {
        Map<String, Object> getViewModel(TRemoteData remoteData, Map<String, String> macroParameters);
    }
}
