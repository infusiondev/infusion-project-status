package com.infusion.confluence.plugins.projectstatus.macro;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.collect.ImmutableSortedSet;
import com.infusion.confluence.plugins.projectstatus.data.IssueGroupDetails;
import com.infusion.confluence.plugins.projectstatus.data.ProjectStatusSummary;
import com.infusion.confluence.plugins.projectstatus.data.QueryParameters;
import com.infusion.confluence.plugins.projectstatus.stats.EpicStats;
import com.infusion.confluence.plugins.projectstatus.stats.MilestoneStats;
import com.infusion.confluence.plugins.projectstatus.stats.ProjectSummary;
import com.infusion.confluence.plugins.projectstatus.util.Util;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

public class RemoteDataProvider {
    private final MilestoneStats milestoneStats;
    private final EpicStats epicStats;
    private final ProjectSummary projectSummary;

    public RemoteDataProvider(MilestoneStats milestoneStats,
                              EpicStats epicStats,
                              ProjectSummary projectSummary) {
        this.milestoneStats = milestoneStats;
        this.epicStats = epicStats;
        this.projectSummary = projectSummary;
    }

    public ProjectStatusSummary getMilestonesSummaryRemoteData(Map<String, String> macroParameters) {
        return getData(
                "milestones summary",
                macroParameters,
                (projectId, queryParams) ->
                        projectSummary.getMilestonesSummary(
                                projectId,
                                Util.getListMacroParameter(macroParameters, "milestoneDetailsView"),
                                queryParams));
    }

    public ProjectStatusSummary getEpicsSummaryRemoteData(Map<String, String> macroParameters) {
        // TODO parameters validation, let the service crash when parameters invalid
        return getData(
                "epics summary",
                macroParameters,
                (projectId, queryParams) ->
                        projectSummary.getEpicsSummary(
                                projectId,
                                Util.getListMacroParameter(macroParameters, "epicDetailsView"),
                                queryParams));
    }

    public ProjectStatusSummary getProjectSummaryRemoteData(Map<String, String> macroParameters) {
        return getData(
                "project summary",
                macroParameters,
                (projectId, queryParams) ->
                        projectSummary.getOverallProjectStatusSummary(
                                projectId,
                                queryParams));
    }

    public List<IssueGroupDetails> getEpicsDetailsRemoteData(Map<String, String> macroParameters) {
        return getData(
                "epics details",
                macroParameters,
                (projectId, queryParams) ->
                    epicStats.getEpicDetails(
                        projectId,
                        Util.getListMacroParameter(macroParameters, "epicDetailsView"),
                        queryParams));
    }

    public List<IssueGroupDetails> getMilestonesDetailsRemoteData(Map<String, String> macroParameters) {
        return getData(
                "milestones details",
                macroParameters,
                (projectId, queryParams) ->
                    milestoneStats.getMilestoneDetails(
                        projectId,
                        Util.getListMacroParameter(macroParameters, "milestoneDetailsView"),
                        queryParams));
    }

    private <T> T getData(String id, Map<String, String> macroParameters, RemoteDataGetterFunctionalInterface<T> getter) {
        String projectId = Util.getSimpleMacroParameter(macroParameters, "jiraProject");
        QueryParameters queryParameters = new QueryParameters();
        queryParameters.setExcludedStatuses(
                ImmutableSortedSet.copyOf(Util.getListMacroParameter(macroParameters, "excludedStatuses")));
        queryParameters.setIncludedLabels(
                ImmutableSortedSet.copyOf(Util.getListMacroParameter(macroParameters, "includedLabels")));

        try {
            return getter.getData(projectId, queryParameters);
        } catch (ResponseException e) {
            throw new RuntimeException(MessageFormat.format("[{0}] Error fetching content from JIRA [code 1]", id));
        } catch (CredentialsRequiredException e) {
            throw new RuntimeException(MessageFormat.format("[{0}] Error fetching content from JIRA [code 2]", id));
        }
    }

    private interface RemoteDataGetterFunctionalInterface<TData> {
        TData getData(String projectId, QueryParameters queryParameters)
                throws ResponseException, CredentialsRequiredException;
    }
}