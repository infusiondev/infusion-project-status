package com.infusion.confluence.plugins.projectstatus.macro;

import com.atlassian.sal.api.message.I18nResolver;
import com.infusion.confluence.plugins.projectstatus.data.IssueGroupDetails;
import com.infusion.confluence.plugins.projectstatus.data.IssueStatusClassificationCount;
import com.infusion.confluence.plugins.projectstatus.data.JiraWorkProgress;
import com.infusion.confluence.plugins.projectstatus.data.ProjectStatusSummary;
import com.infusion.confluence.plugins.projectstatus.services.JiraConnectorService;
import com.infusion.confluence.plugins.projectstatus.util.TemplateUtil;
import com.infusion.confluence.plugins.projectstatus.util.Util;
import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.util.StringUtils;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ViewModelFactory {
    private final JiraConnectorService jiraConnectorService;
    private final I18nResolver i18n;

    public ViewModelFactory(JiraConnectorService jiraConnectorService, I18nResolver i18n) {
        this.jiraConnectorService = jiraConnectorService;
        this.i18n = i18n;
    }

    public Map<String, Object> createMilestonesSummaryViewModel(ProjectStatusSummary remoteData, Map<String, String> parameters) {
        Map<String, Object> viewModel = createProjectSummaryViewModel(remoteData, parameters);
        viewModel.put("selectedMilestones", Util.getListMacroParameter(parameters, "milestoneDetailsView"));
        return viewModel;
    }

    public Map<String, Object> createEpicsSummaryViewModel(ProjectStatusSummary remoteData, Map<String, String> parameters) {
        Map<String, Object> viewModel = createProjectSummaryViewModel(remoteData, parameters);
        viewModel.put("selectedEpics", Util.getListMacroParameter(parameters, "epicDetailsView"));
        return viewModel;
    }

    public Map<String, Object> createProjectSummaryViewModel(ProjectStatusSummary remoteData, Map<String, String> parameters) {
        Map<String, Object> viewModel = createBasicViewModel(parameters);

        viewModel.put("romProgressPercent", (int) Math.floor(remoteData.getRomProgress() * 100));

        if (remoteData.getDueDate() != null) {
            viewModel.put("formattedDueDate", DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US).format(remoteData.getDueDate()));
        }

        return viewModel;
    }

    public Map<String, Object> createEpicsDetailsViewModel(
            List<IssueGroupDetails> remoteData, Map<String, String> parameters) {
        Map<String, Object> viewModel = createAbstractDetailsViewModel(remoteData, parameters, this::toEpicDetailViewModel);
        viewModel.put("caption", parameters.get("epicCaption"));
        return viewModel;
    }

    public Map<String, Object> createMilestonesDetailsViewModel(
            List<IssueGroupDetails> remoteData, Map<String, String> parameters) {
        Map<String, Object> viewModel = createAbstractDetailsViewModel(remoteData, parameters);
        return viewModel;
    }

    private Map<String, Object> createAbstractDetailsViewModel(
            List<IssueGroupDetails> remoteData, Map<String, String> parameters) {
        return createAbstractDetailsViewModel(remoteData, parameters, this::toViewModel);
    }

    private Map<String, Object> createAbstractDetailsViewModel(
            List<IssueGroupDetails> remoteData, Map<String, String> parameters, Function<IssueGroupDetails, Map<String, Object>> detailMapper) {
        Map<String, Object> viewModel = createBasicViewModel(parameters);

        List<Map<String, Object>> items = remoteData.stream().map(detailMapper).collect(Collectors.toList());

        viewModel.put("items", items);
        viewModel.put("showHours", Boolean.parseBoolean(parameters.get("showHours")));

        return viewModel;
    }

    private Map<String, Object> createBasicViewModel(Map<String, String> parameters) {
        Map<String, Object> viewModel = new HashMap<>();

        viewModel.put("width", parameters.get("width"));
        viewModel.put("border", Boolean.parseBoolean(parameters.get("border")));
        viewModel.put("velStringUtils", new StringUtils());
        viewModel.put("velMathUtils", new MathTool());
        viewModel.put("infTemplateUtils", new TemplateUtil());

        return viewModel;
    }

    private Map<String, Object> toViewModel(IssueStatusClassificationCount dto) {
        Map<String, Object> viewModel = new HashMap<>();

        viewModel.put("doneCount", dto.getIssuesDoneCount());
        viewModel.put("inProgressCount", dto.getIssuesInProgressCount());
        viewModel.put("totalCount", dto.getTotalCount());

        return viewModel;
    }

    private Map<String, Object> toViewModel(JiraWorkProgress workProgress) {
        Map<String, Object> viewModel = new HashMap<>();

        double hoursTodo = workProgress.getHoursTotal() - workProgress.getHoursWorked();

        viewModel.put("hoursWorked", (int) Math.floor(workProgress.getHoursWorked()));
        viewModel.put("hoursDone", (int) Math.floor(workProgress.getHoursDone()));
        viewModel.put("hoursInProgress", (int) Math.floor(workProgress.getHoursInProgress()));
        viewModel.put("hoursToDo", (int) Math.floor(hoursTodo));
        viewModel.put("hoursTotal", (int) Math.floor(workProgress.getHoursTotal()));
        viewModel.put("hoursDonePercent", Util.calculatePercent(workProgress.getHoursTotal(), workProgress.getHoursDone()));
        viewModel.put("hoursInProgressPercent", Util.calculatePercent(workProgress.getHoursTotal(), workProgress.getHoursInProgress()));
        viewModel.put("hoursToDoPercent", Util.calculatePercent(workProgress.getHoursTotal(), hoursTodo));
        viewModel.put("hoursWorkedPercent", Util.calculatePercent(workProgress.getHoursTotal(), workProgress.getHoursWorked()));

        return viewModel;
    }

    private Map<String, Object> toEpicDetailViewModel(IssueGroupDetails issueGroupDetails) {
        Map<String, Object> baseViewModel = toViewModel(issueGroupDetails);
        URI jiraUri = jiraConnectorService.getJiraApplicationLink().getDisplayUrl();
        List<Map<String, String>> extraLinks = new ArrayList<>();
        String groupKey = issueGroupDetails.getKey();
        URI epicUri = UriBuilder.fromUri(jiraUri).path("browse").path(groupKey).build();
        URI issuesUri = UriBuilder.fromUri(jiraUri).path("issues").queryParam("jql",
                MessageFormat.format("\"Epic Link\"={0}", groupKey)).build();
        URI issuesActiveSprintUri = UriBuilder.fromUri(jiraUri).path("issues").queryParam("jql",
                MessageFormat.format("\"Epic Link\"={0} AND Sprint IN openSprints() AND Sprint NOT IN futureSprints()", groupKey)).build();

        extraLinks.add(createLink(
                i18n.getText("infusion-project-status.epics-details.extra-links.epic-link.caption"),
                epicUri.toString()));
        extraLinks.add(createLink(
                i18n.getText("infusion-project-status.epics-details.extra-links.child-issues-link.caption"),
                issuesUri.toString()));
        extraLinks.add(createLink(
                i18n.getText("infusion-project-status.epics-details.extra-links.child-issues-active-sprint-link.caption"),
                issuesActiveSprintUri.toString()));

        baseViewModel.put("extraLinks", extraLinks);

        return baseViewModel;
    }

    private Map<String, Object> toViewModel(IssueGroupDetails issueGroupDetails) {
        Map<String, Object> details = new HashMap<>();

        Map<String, Object> counts = new HashMap<>();
        counts.put("all", toViewModel(issueGroupDetails.getIssueStatusClassificationCount()));
        counts.put("activeSprint", toViewModel(issueGroupDetails.getIssueStatusClassificationActiveSprintCount()));
        counts.put("outsideActiveSprint", toViewModel(issueGroupDetails.getIssueStatusClassificationOutsideActiveSprintCount()));

        Map<String, Object> completion = new HashMap<>();
        completion.put("completionPercent", (int) Math.floor(issueGroupDetails.getRomProgress() * 100));

        details.put("completion", completion);
        details.put("displayName", issueGroupDetails.getDisplayName());
        details.put("counts", counts);
        details.put("times", toViewModel(issueGroupDetails.getWorkProgress()));

        return details;
    }

    private Map<String, String> createLink(String caption, String href) {
        Map<String, String> link = new HashMap<>();
        link.put("linkCaption", caption);
        link.put("linkHref", href);
        return link;
    }
}
