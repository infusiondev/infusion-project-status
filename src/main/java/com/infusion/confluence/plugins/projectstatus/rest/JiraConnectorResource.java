package com.infusion.confluence.plugins.projectstatus.rest;

import com.atlassian.applinks.api.ApplicationLink;
import com.google.common.collect.ImmutableMap;
import com.infusion.confluence.plugins.projectstatus.services.JiraConnectorService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("jira-connector")
@Produces({MediaType.APPLICATION_JSON})
public class JiraConnectorResource {

    private final JiraConnectorService jiraConnectorService;

    public JiraConnectorResource(JiraConnectorService jiraConnectorService) {
        this.jiraConnectorService = jiraConnectorService;
    }

    @GET
    @Path("application-link")
    public Response getJiraApplicationLink() {
        ApplicationLink jiraApplicationLink = jiraConnectorService.getJiraApplicationLink();

        // Include application link id only for now
        return Response.ok(ImmutableMap.of("id", jiraApplicationLink.getId().get())).build();
    }
}
