package com.infusion.confluence.plugins.projectstatus.rest;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.infusion.confluence.plugins.projectstatus.services.JiraRestClientService;
import com.infusion.confluence.plugins.projectstatus.util.ExceptionHelper;
import com.infusion.confluence.plugins.projectstatus.util.Lazy;
import com.infusion.confluence.plugins.projectstatus.util.Util;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.google.common.base.Preconditions.checkNotNull;

@Path("macro-editor")
@Produces({ MediaType.APPLICATION_JSON})
public class MacroEditorResource {
    private final JiraRestClientService jiraRestClientService;
    private final Lazy<JsonArray> profieldsFields = new Lazy<>(this::getProfieldsFields);

    private final static String OA_CODE_FIELD_NAME = "ProjectOpenAirCode";
    private final static String PROJECT_INFO_REST_URL = "/rest/api/2/project/{0}";
    private final static String PROFIELDS_FIELD_VALUE_REST_URL = "/rest/profields/api/1.0/field/{0}/value/{1}";
    private final static String PROJECT_MILESTONES_REST_URL = "http://tdintrask01.corp.infusiondev.com/jirawebclient/api/jira/GetMilestones?project={0}";
    private final static String PROFIELDS_FIELD_LIST_REST_URL = "/rest/profields/api/1.0/field";

    public MacroEditorResource(JiraRestClientService jiraRestClientService) {
        this.jiraRestClientService = jiraRestClientService;
    }

    @GET
    @Path("suggestions/milestones")
    public Response getMilestonesSuggestions(@QueryParam("projectId") final String projectKey) {
        checkNotNull(projectKey);

        JsonObject projectInfo = jiraRestClientService
                .getJson(PROJECT_INFO_REST_URL, Stream.of(projectKey)).getAsJsonObject();
        JsonArray profieldsFields = this.profieldsFields.getValue();

        Optional<JsonObject> oaCodeFieldJson = StreamSupport
                .stream(profieldsFields.spliterator(), false)
                .map(fieldJsonElement -> fieldJsonElement.getAsJsonObject())
                .filter(fieldJsonObject -> fieldJsonObject.get("name").getAsString().equals(OA_CODE_FIELD_NAME))
                .findFirst();

        if (!oaCodeFieldJson.isPresent()) {
            throw ExceptionHelper.getJiraConfigurationException(OA_CODE_FIELD_NAME + " project field is missing");
        }

        if (!projectInfo.has("id")) {
            throw ExceptionHelper.getProjectInfoRetrievalException(projectKey);
        }

        // a paranoid conversion to make sure it's a number
        String projectId = String.valueOf(projectInfo.get("id").getAsLong());
        String oaCodeFieldId = String.valueOf(oaCodeFieldJson.get().get("id").getAsLong());

        JsonObject profieldsOAFieldValueJson = jiraRestClientService.getJson(
                PROFIELDS_FIELD_VALUE_REST_URL,
                Stream.of(oaCodeFieldId, projectId))
            .getAsJsonObject();

        if (!profieldsOAFieldValueJson.has("value")) {
            throw ExceptionHelper.getProfieldsProjectInfoRetrievalException(projectKey, oaCodeFieldId);
        }

        String oaProjectCode = profieldsOAFieldValueJson.get("value").getAsString();

        URL milestonesUrl = Util.getURL(PROJECT_MILESTONES_REST_URL, oaProjectCode);

        JsonArray milestonesJsonArray = new JsonParser()
                .parse(Util.readFromURL(milestonesUrl))
                .getAsJsonArray();

        List<String> milestones = StreamSupport
                .stream(milestonesJsonArray.spliterator(), false)
                .map(milestoneJsonElement -> milestoneJsonElement.getAsJsonObject())
                .map(milestoneJsonObject -> milestoneJsonObject.get("Name").getAsString())
                .collect(Collectors.toList());

        return Response.ok(milestones).build();
    }

    private JsonArray getProfieldsFields() {
        return jiraRestClientService.getJson(PROFIELDS_FIELD_LIST_REST_URL).getAsJsonArray();
    }
}
