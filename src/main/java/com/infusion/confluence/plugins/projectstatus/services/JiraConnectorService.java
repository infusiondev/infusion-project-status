package com.infusion.confluence.plugins.projectstatus.services;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;

public class JiraConnectorService {
    private final ApplicationLinkService appLinkService;

    public JiraConnectorService(ApplicationLinkService appLinkService) {
        this.appLinkService = appLinkService;
    }

    public ApplicationLink getJiraApplicationLink() {
        return appLinkService.getPrimaryApplicationLink(JiraApplicationType.class);
    }
}
