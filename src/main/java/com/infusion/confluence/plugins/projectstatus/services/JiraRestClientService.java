package com.infusion.confluence.plugins.projectstatus.services;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ResponseException;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.infusion.confluence.plugins.projectstatus.AbstractJiraClient;
import com.infusion.confluence.plugins.projectstatus.util.ExceptionHelper;
import com.infusion.confluence.plugins.projectstatus.util.Util;

import java.net.URI;
import java.net.URL;
import java.util.stream.Stream;

public class JiraRestClientService extends AbstractJiraClient {
    public JiraRestClientService(JiraConnectorService jiraConnectorService) {
        super(jiraConnectorService);
    }

    public JsonElement getJson(String url) {
        return getJson(url, Stream.empty());
    }

    public JsonElement getJson(String urlFormat, Stream<String> pathParams) {
        try {
            URI url = Util.getURI(urlFormat, pathParams.map(Util::prepareUrlElement).toArray());

            ApplicationLinkRequest request = createRequest(Request.MethodType.GET, url.getPath());
            String response = executeRequest(request);
            return new JsonParser().parse(response);
        } catch (CredentialsRequiredException | ResponseException e) {
            throw ExceptionHelper.getJiraCommunicationException(e);
        }
    }
}
