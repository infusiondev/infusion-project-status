package com.infusion.confluence.plugins.projectstatus.stats;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.sal.api.net.ResponseException;
import com.infusion.confluence.plugins.projectstatus.JiraFieldsRepository;
import com.infusion.confluence.plugins.projectstatus.JiraIssueUtils;
import com.infusion.confluence.plugins.projectstatus.JiraIssuesManager;
import com.infusion.confluence.plugins.projectstatus.JqlBuilder;
import com.infusion.confluence.plugins.projectstatus.aggregator.IssueGroupDetailsAggregator;
import com.infusion.confluence.plugins.projectstatus.aggregator.IssuesAggregator;
import com.infusion.confluence.plugins.projectstatus.data.*;
import com.infusion.confluence.plugins.projectstatus.util.InfusionCollectors;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import java.text.MessageFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.infusion.confluence.plugins.projectstatus.Constants.*;
import static java.util.function.Function.identity;
import static org.springframework.util.StringUtils.quote;

public class EpicStats {

    private static final Logger LOG = LoggerFactory.getLogger(EpicStats.class);

    private final JiraIssuesManager jiraIssuesManager;
    private final IssuesAggregator<Double> romProgressIssuesAggregator;
    private final IssuesAggregator<JiraWorkProgress> workProgressIssuesAggregator;
    private final IssuesAggregator<IssueStatusClassificationCount> issueStatusClassificationCountAggregator;
    private final JiraFieldsRepository jiraFieldsRepository;
    private final JiraIssueUtils issueUtils;
    private final Predicate<JiraIssue> isEpicPredicate;

    public EpicStats(JiraIssuesManager jiraIssuesManager,
                     @Qualifier("romProgressIssuesAggregator") IssuesAggregator<Double> romProgressIssuesAggregator,
                     @Qualifier("workProgressIssuesAggregator") IssuesAggregator<JiraWorkProgress> workProgressIssuesAggregator,
                     @Qualifier("issueStatusClassificationCountAggregator") IssuesAggregator<IssueStatusClassificationCount> issueStatusClassificationCountAggregator,
                     JiraFieldsRepository jiraFieldsRepository,
                     JiraIssueUtils issueUtils) {
        this.jiraIssuesManager = jiraIssuesManager;
        this.romProgressIssuesAggregator = romProgressIssuesAggregator;
        this.workProgressIssuesAggregator = workProgressIssuesAggregator;
        this.issueStatusClassificationCountAggregator = issueStatusClassificationCountAggregator;
        this.jiraFieldsRepository = jiraFieldsRepository;
        this.issueUtils = issueUtils;
        this.isEpicPredicate = input -> issueUtils.issueType(input).equals(EPIC_ISSUE_TYPE_NAME);
    }

    public List<JiraIssue> getEpics(final String projectId) throws CredentialsRequiredException, ResponseException, TypeNotInstalledException {
        checkNotNull(projectId, "projectId cannot be empty");

        String epicsJql = new JqlBuilder()
                .projectKeys(projectId)
                .issueTypes(quote(EPIC_ISSUE_TYPE_NAME))
                .buildRawJql();
        JiraField epicNameField = jiraFieldsRepository.get(EPIC_NAME_FIELD_NAME);
        List<JiraIssue> epics = jiraIssuesManager.queryForIssues(epicsJql, epicNameField.getId());
        for (JiraIssue epic : epics) {
            epic.getFields().put(EPIC_NAME_FIELD_NAME, epic.getFields().get(epicNameField.getId()));
        }
        Collections.sort(epics, (o1, o2) -> {
            String epicName1 = (String) o1.getFields().get(EPIC_NAME_FIELD_NAME);
            String epicName2 = (String) o2.getFields().get(EPIC_NAME_FIELD_NAME);
            return epicName1.compareToIgnoreCase(epicName2);
        });
        return epics;
    }

    public IssueStatusClassificationCount getEpicsSummary(final String projectId,
                                                          final QueryParameters params) throws CredentialsRequiredException, ResponseException {
        checkNotNull(projectId, "projectId cannot be empty");

        String epicsJql = new JqlBuilder()
                .projectKeys(projectId)
                .issueTypes(quote(EPIC_ISSUE_TYPE_NAME))
                .apply(params::addFilters)
                .buildRawJql();
        List<JiraIssue> epics = jiraIssuesManager.queryForIssues(epicsJql, STATUS_FIELD_NAME);
        IssueStatusClassificationCount result = issueStatusClassificationCountAggregator.aggregate(epics);

        return result;
    }

    public List<IssueGroupDetails> getEpicDetails(final String projectId,
                                                  final List<String> epicKeys,
                                                  final QueryParameters params) throws CredentialsRequiredException, ResponseException {
        checkNotNull(projectId, "projectId cannot be empty");

        List<JiraIssue> epics = new ArrayList<>();
        List<JiraIssue> issues = new ArrayList<>();

        if (!fetchEpicsAndIssuesWithDetails(projectId, epicKeys, params, epics::addAll, issues::addAll)) {
            return Collections.emptyList();
        }

        final JiraField epicLinkField = jiraFieldsRepository.get(EPIC_LINK_FIELD_NAME);
        final JiraField epicNameField = jiraFieldsRepository.get(EPIC_NAME_FIELD_NAME);

        final Map<String, JiraIssue> epicsIndex = epics.stream().filter(isEpicPredicate).collect(
                Collectors.toMap(JiraIssue::getKey, identity()));

        // TODO IssueGroupDetailsAggregator Should have a factory and be injected, leave it this way for now
        IssueGroupDetailsAggregator issueGroupDetailsAggregator = new IssueGroupDetailsAggregator(
                romProgressIssuesAggregator,
                workProgressIssuesAggregator,
                issueStatusClassificationCountAggregator,
                issue -> issue.getFieldValue(epicLinkField),
                issue -> {
                    String epicKey = issue.getFieldValue(epicLinkField);
                    return epicsIndex.get(epicKey).getFieldValue(epicNameField);
                },
                issueUtils
        );

        Collection<IssueGroupDetails> aggregates = issueGroupDetailsAggregator.aggregate(issues);
        List<IssueGroupDetails> results = new ArrayList<>(aggregates);
        Collections.sort(results, IssueGroupDetails.COMPARATOR);
        return results;
    }

    private boolean fetchEpicsAndIssuesWithDetails(
            String projectId,
            List<String> epicKeys,
            QueryParameters params,
            Consumer<Collection<? extends JiraIssue>> epicConsumer,
            Consumer<Collection<? extends JiraIssue>> issueConsumer
    ) throws CredentialsRequiredException, ResponseException {

        checkNotNull(projectId, "projectId cannot be empty");

        if (epicKeys == null || epicKeys.isEmpty() || (epicKeys.size() == 1 && StringUtils.isBlank(epicKeys.get(0)))) {
            return false;
        }

        // Perform two queries combined with OR to avoid extra call

        JiraField epicLinkField = jiraFieldsRepository.get(EPIC_LINK_FIELD_NAME);
        JiraField epicNameField = jiraFieldsRepository.get(EPIC_NAME_FIELD_NAME);
        JiraField romEstimateField = jiraFieldsRepository.get(ROM_ESTIMATE_FIELD_NAME);
        JiraField sprintField = jiraFieldsRepository.get(SPRINT_FIELD_NAME);
        String[] issueKeys = epicKeys.toArray(new String[epicKeys.size()]);

        String issuesJql = new JqlBuilder()
                .projectKeys(projectId)
                .issueTypes(STORY_ISSUE_TYPE_NAME, BUG_ISSUE_TYPE)
                .put(quote(EPIC_LINK_FIELD_NAME), issueKeys)
                .apply(params::addFilters)
                .buildRawJql();

        String epicsJql = new JqlBuilder()
                .projectKeys(projectId)
                .issueTypes(EPIC_ISSUE_TYPE_NAME)
                .issueKeys(issueKeys)
                .buildRawJql();

        String jql = MessageFormat.format("({0}) OR ({1})", issuesJql, epicsJql);

        String[] fetchFields = {
                romEstimateField.getId(),
                epicLinkField.getId(),
                epicNameField.getId(),
                sprintField.getId(),
                AGGREGATE_PROGRESS_FIELD_NAME,
                STATUS_FIELD_NAME,
                ISSUE_TYPE_FIELD_NAME,
        };


        List<JiraIssue> epicsAndIssues = jiraIssuesManager.queryForIssues(jql, fetchFields);

        // Split result and feed two consumers

        epicsAndIssues.stream().collect(InfusionCollectors.partitioningBy(isEpicPredicate, epicConsumer, issueConsumer));

        return !epicsAndIssues.isEmpty();
    }

}
