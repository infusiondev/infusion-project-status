package com.infusion.confluence.plugins.projectstatus.stats;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.sal.api.net.ResponseException;
import com.infusion.confluence.plugins.projectstatus.JiraFieldsRepository;
import com.infusion.confluence.plugins.projectstatus.JiraIssueUtils;
import com.infusion.confluence.plugins.projectstatus.JiraIssuesManager;
import com.infusion.confluence.plugins.projectstatus.JqlBuilder;
import com.infusion.confluence.plugins.projectstatus.aggregator.IssueGroupDetailsAggregator;
import com.infusion.confluence.plugins.projectstatus.aggregator.IssuesAggregator;
import com.infusion.confluence.plugins.projectstatus.aggregator.MilestonesProgressAggregator;
import com.infusion.confluence.plugins.projectstatus.data.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.infusion.confluence.plugins.projectstatus.Constants.*;

public class MilestoneStats {

    private final static Logger log = LoggerFactory.getLogger(MilestoneStats.class);

    private final JiraIssuesManager jiraIssuesManager;
    private final IssuesAggregator<Double> romProgressIssuesAggregator;
    private final IssuesAggregator<JiraWorkProgress> workProgressIssuesAggregator;
    private final IssuesAggregator<IssueStatusClassificationCount> issueStatusClassificationCountAggregator;
    private final JiraFieldsRepository fieldsRepository;
    private final JiraIssueUtils issueUtils;

    public MilestoneStats(JiraIssuesManager jiraIssuesManager,
                          @Qualifier("romProgressIssuesAggregator") IssuesAggregator<Double> romProgressIssuesAggregator,
                          @Qualifier("workProgressIssuesAggregator") IssuesAggregator<JiraWorkProgress> workProgressIssuesAggregator,
                          @Qualifier("issueStatusClassificationCountAggregator") IssuesAggregator<IssueStatusClassificationCount> issueStatusClassificationCountAggregator,
                          JiraFieldsRepository fieldsRepository,
                          JiraIssueUtils issueUtils) {
        this.jiraIssuesManager = jiraIssuesManager;
        this.romProgressIssuesAggregator = romProgressIssuesAggregator;
        this.workProgressIssuesAggregator = workProgressIssuesAggregator;
        this.issueStatusClassificationCountAggregator = issueStatusClassificationCountAggregator;
        this.fieldsRepository = fieldsRepository;
        this.issueUtils = issueUtils;
    }

    public List<IssueGroupDetails> getMilestoneDetails(final String projectId,
                                                       final List<String> milestoneKeys,
                                                       final QueryParameters params)
            throws CredentialsRequiredException, ResponseException {

        checkNotNull(projectId, "projectId cannot be empty");

        final JiraField romEstimateField = fieldsRepository.get(ROM_ESTIMATE_FIELD_NAME);
        final JiraField milestonesField = fieldsRepository.get(MILESTONES_FIELD_NAME);
        final JiraField sprintField = fieldsRepository.get(SPRINT_FIELD_NAME);
        final Function<JiraIssue, String> milestoneNameGetter = issue -> issue.getFieldValue(milestonesField);

        List<JiraIssue> issues = queryForMilestones(projectId, params,
                romEstimateField.getId(),
                milestonesField.getId(),
                sprintField.getId(),
                AGGREGATE_PROGRESS_FIELD_NAME,
                STATUS_FIELD_NAME);

        // There is something wrong with our Jira and we can't query for issues from milestones
        // as a workaround we will filter result
        if (milestoneKeys != null) {
            issues = issues.stream().filter(input -> {
                String milestone = milestoneNameGetter.apply(input);
                return milestone != null && milestoneKeys.contains(milestone);
            }).collect(Collectors.toList());
        }

        if (issues == null || issues.isEmpty()) {
            return Collections.emptyList();
        }

        // TODO IssueGroupDetailsAggregator Should have a factory and be injected, leave it this way for now
        IssueGroupDetailsAggregator issueGroupDetailsAggregator = new IssueGroupDetailsAggregator(
                romProgressIssuesAggregator,
                workProgressIssuesAggregator,
                issueStatusClassificationCountAggregator,
                milestoneNameGetter,
                milestoneNameGetter,
                issueUtils
        );

        Collection<IssueGroupDetails> aggregates = issueGroupDetailsAggregator.aggregate(issues);
        List<IssueGroupDetails> results = new ArrayList<>(aggregates);
        Collections.sort(results, IssueGroupDetails.COMPARATOR);
        return results;
    }

    public Set<String> getMilestones(final String projectId) throws TypeNotInstalledException, CredentialsRequiredException, ResponseException {
        final JiraField milestonesField = fieldsRepository.get(MILESTONES_FIELD_NAME);
        List<JiraIssue> issues = queryForMilestones(projectId, QueryParameters.DEFAULT, milestonesField.getId());

        return issues.stream().
                map(input -> input.getFieldValue(milestonesField)).
                sorted().
                collect(Collectors.toSet());
    }

    public IssueStatusClassificationCount getMilestonesSummary(String projectId, QueryParameters params) throws CredentialsRequiredException, ResponseException {
        checkNotNull(projectId, "projectId cannot be empty");

        final JiraField milestonesField = fieldsRepository.get(MILESTONES_FIELD_NAME);

        // there are no entities for milestones - it's just a custom field in issue

        List<JiraIssue> issuesWithMilestones = queryForMilestones(projectId, params, STATUS_FIELD_NAME, milestonesField.getId());

        MilestonesProgressAggregator milestonesProgressAggregator = new MilestonesProgressAggregator(
                issueStatusClassificationCountAggregator,
                input -> input.getFieldValue(milestonesField));

        return milestonesProgressAggregator.aggregate(issuesWithMilestones);
    }

    private List<JiraIssue> queryForMilestones(String projectId, QueryParameters params, String... fields) throws CredentialsRequiredException, ResponseException {
        checkNotNull(projectId, "projectId cannot be empty");

        String jql = new JqlBuilder()
                .projectKeys(projectId)
                .issueTypes(STORY_ISSUE_TYPE_NAME, BUG_ISSUE_TYPE)
                .notEmpty(MILESTONES_FIELD_NAME)
                .apply(params::addFilters)
                .buildRawJql();
        return jiraIssuesManager.queryForIssues(jql, fields);
    }
}
