package com.infusion.confluence.plugins.projectstatus.stats;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.sal.api.net.ResponseException;
import com.infusion.confluence.plugins.projectstatus.JiraAgileManager;
import com.infusion.confluence.plugins.projectstatus.JiraFieldsRepository;
import com.infusion.confluence.plugins.projectstatus.JiraIssuesManager;
import com.infusion.confluence.plugins.projectstatus.JqlBuilder;
import com.infusion.confluence.plugins.projectstatus.aggregator.IssuesAggregator;
import com.infusion.confluence.plugins.projectstatus.data.*;
import com.infusion.confluence.plugins.projectstatus.util.ExceptionHelper;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.infusion.confluence.plugins.projectstatus.Constants.*;
import static org.springframework.util.StringUtils.quote;

public class ProjectSummary {

    private final JiraIssuesManager jiraIssuesManager;
    private final JiraAgileManager jiraAgileManager;
    private final IssuesAggregator<Double> romProgressIssuesAggregator;
    private final JiraFieldsRepository fieldsRepository;

    public ProjectSummary(JiraIssuesManager jiraIssuesManager,
                          JiraAgileManager jiraAgileManager,
                          @Qualifier("romProgressIssuesAggregator") IssuesAggregator<Double> romProgressIssuesAggregator,
                          JiraFieldsRepository fieldsRepository) {
        this.jiraIssuesManager = jiraIssuesManager;
        this.jiraAgileManager = jiraAgileManager;
        this.romProgressIssuesAggregator = romProgressIssuesAggregator;
        this.fieldsRepository = fieldsRepository;
    }

    public ProjectStatusSummary getOverallProjectStatusSummary(final String projectId, final QueryParameters queryParameters) {
        checkNotNull(projectId, "projectId cannot be empty");

        Double progress = getProjectsProgress(projectId, null, null, queryParameters);
        ProjectStatusSummary summary = new ProjectStatusSummary();
        summary.setRomProgress(progress);

        try {
            summary.setDueDate(jiraAgileManager.queryForDueDate(projectId));
        } catch (CredentialsRequiredException|ResponseException e) {
            throw ExceptionHelper.getJiraCommunicationException(e);
        }
        return summary;
    }

    public ProjectStatusSummary getEpicsSummary(final String projectId,
                                                List<String> epicKeys,
                                                final QueryParameters queryParameters) {
        checkNotNull(projectId, "projectId cannot be empty");

        Double progress = getProjectsProgress(projectId, epicKeys, null, queryParameters);
        ProjectStatusSummary summary = new ProjectStatusSummary();
        summary.setRomProgress(progress);
        return summary;
    }

    public ProjectStatusSummary getMilestonesSummary(final String projectId,
                                                List<String> milestones,
                                                final QueryParameters queryParameters) {
        checkNotNull(projectId, "projectId cannot be empty");

        Double progress = getProjectsProgress(projectId, null, milestones, queryParameters);
        ProjectStatusSummary summary = new ProjectStatusSummary();
        summary.setRomProgress(progress);
        return summary;
    }

    private Double getProjectsProgress(final String projectId,
                                       List<String> epicKeys,
                                       List<String> milestones,
                                       final QueryParameters queryParameters) {
        checkNotNull(projectId, "projectId cannot be empty");

        String[] epicKeysArray = epicKeys != null ? epicKeys.toArray(new String[0]) : new String[0];

        JqlBuilder builder = new JqlBuilder()
                .projectKeys(projectId)
                .issueTypes(STORY_ISSUE_TYPE_NAME, BUG_ISSUE_TYPE);

        if (epicKeys != null && epicKeys.size() > 0) {
            builder = builder.put(quote(EPIC_LINK_FIELD_NAME), epicKeysArray);
        }

        if (milestones != null && milestones.size() > 0) {
            builder = builder.notEmpty(MILESTONES_FIELD_NAME);
        }

        String jql = builder
                .apply(queryParameters::addFilters)
                .buildRawJql();

        JiraField romEstimateField = fieldsRepository.get(ROM_ESTIMATE_FIELD_NAME);
        JiraField sprintField = fieldsRepository.get(SPRINT_FIELD_NAME);
        final JiraField milestonesField = fieldsRepository.get(MILESTONES_FIELD_NAME);
        List<JiraIssue> issues;

        try {
            issues = jiraIssuesManager.queryForIssues(jql,
                    romEstimateField.getId(),
                    sprintField.getId(),
                    milestonesField.getId(),
                    STATUS_FIELD_NAME,
                    AGGREGATE_PROGRESS_FIELD_NAME);
        } catch (CredentialsRequiredException|ResponseException e) {
            throw ExceptionHelper.getJiraCommunicationException(e);
        }

        if (milestones != null && milestones.size() > 0) {
                issues = issues.stream().filter(issue -> {
                String milestone = issue.getFieldValue(milestonesField);
                return milestone != null && milestones.contains(milestone);
            }).collect(Collectors.toList());
        }

        return romProgressIssuesAggregator.aggregate(issues);
    }
}
