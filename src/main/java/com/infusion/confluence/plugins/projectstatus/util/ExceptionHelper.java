package com.infusion.confluence.plugins.projectstatus.util;

import java.text.MessageFormat;

public final class ExceptionHelper {
    private ExceptionHelper() {
    }

    public static RuntimeException getCommunicationException(Exception e) {
        return new RuntimeException("Communication error occurred", e);
    }

    public static RuntimeException getJiraCommunicationException(Exception e) {
        return new RuntimeException("An error occurred while contacting jira", e);
    }

    public static RuntimeException getThirdPartyCommunicationException(String detailMessage) {
        String message = MessageFormat.format(
                "An error occurred while contacting a third party service: {0}", detailMessage);
        return new RuntimeException(message);
    }

    public static RuntimeException getMalformedUrlException() {
        return new RuntimeException("The URL has an incorrect format");
    }

    public static RuntimeException getJiraConfigurationException(String jiraProjectFieldName) {
        return new RuntimeException(
                MessageFormat.format("JIRA configuration error. {0}", jiraProjectFieldName));
    }

    public static RuntimeException getProjectInfoRetrievalException(String projectKey) {
        return new RuntimeException(
                MessageFormat.format("Error fetching project info from JIRA (project key {0})", projectKey));
    }

    public static RuntimeException getProfieldsProjectInfoRetrievalException(String projectId, String fieldId) {
        return new RuntimeException(
                MessageFormat.format("Error fetching Profields project info from JIRA (project id {0}, field id {1})", projectId, fieldId));
    }

    public static RuntimeException getUnspecifiedException(Throwable cause) {
        return new RuntimeException("Unspecified exception", cause);
    }
}
