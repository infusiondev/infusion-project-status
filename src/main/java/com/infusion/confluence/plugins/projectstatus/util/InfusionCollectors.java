package com.infusion.confluence.plugins.projectstatus.util;

import com.google.common.collect.ImmutableSet;

import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class InfusionCollectors {
    /**
     * Returns a collector that partitions stream by predicate and feeds both partitions to separate consumers.
     *
     * @param predicate        a predicate used for classifying input elements
     * @param positiveConsumer a consumer of accepted items
     * @param negativeConsumer a consumer of rejected items
     * @param <T>              type of input elements
     * @return collector instance.
     */
    public static <T> Collector<T, ?, Void> partitioningBy(
            Predicate<? super T> predicate,
            Consumer<Collection<? extends T>> positiveConsumer,
            Consumer<Collection<? extends T>> negativeConsumer
    ) {
        return Collectors.collectingAndThen(Collectors.partitioningBy(predicate), partitions -> {
            positiveConsumer.accept(partitions.get(true));
            negativeConsumer.accept(partitions.get(false));
            return null;
        });
    }

    public static <T, R extends Collection<T>> Collector<T, ?, R> toCollection(Function<Collection<? extends T>, R> factory) {
        return Collectors.collectingAndThen(Collectors.toList(), factory::apply);
    }

    public static <T> Collector<? super T, ?, ImmutableSet<T>> toImmutableSet() {
        return toCollection(ImmutableSet::copyOf);
    }
}
