package com.infusion.confluence.plugins.projectstatus.util;

import java.util.function.Supplier;

public class Lazy<T> {
    private final Supplier<T> supplier;
    private final boolean retryIfNull;

    private boolean isInitialized;
    private T value;

    public Lazy(Supplier<T> supplier)
    {
        this(supplier, false);
    }

    public Lazy(Supplier<T> supplier, boolean retryIfNull) {
        this.supplier = supplier;
        this.retryIfNull = retryIfNull;
    }

    public T getValue() {
        if (isInitialized && (!retryIfNull || value != null)) {
            return value;
        }

        value = supplier.get();
        isInitialized = true;
        return value;
    }
}
