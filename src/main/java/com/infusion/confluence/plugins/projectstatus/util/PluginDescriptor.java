package com.infusion.confluence.plugins.projectstatus.util;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PluginDescriptor {
    private Lazy<Document> pluginDescriptor = new Lazy<>(this::getPluginDescriptor);
    private Lazy<Map<String, Map<String, Node>>> parameterNodesByMacro = new Lazy<>(this::getParameterNodesByMacro);

    public void applyDefaultParameterValues(String macroName, Map<String, String> parameters) {
        Map<String, Node> macroParametersMeta = parameterNodesByMacro.getValue().get(macroName);

        for (String parameterName: macroParametersMeta.keySet()) {
            if (parameters.containsKey(parameterName)) {
                continue;
            }

            String parameterDefaultValue =
                    getAttributeValue(macroParametersMeta.get(parameterName), "default");

            if (parameterDefaultValue == null) {
                continue;
            }

            parameters.put(parameterName, parameterDefaultValue);
        }
    }

    private Document getPluginDescriptor() {
        try {
            DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            return dBuilder.parse(this.getClass().getResourceAsStream("/atlassian-plugin.xml"));
        } catch (SAXException | IOException | ParserConfigurationException e) {
            throw new RuntimeException("Couldn't read plugin descriptor file ('atlassian-plugin.xml')");
        }
    }

    private Map<String, Map<String, Node>> getParameterNodesByMacro() {
        NodeList parameterNodes = pluginDescriptor.getValue().getElementsByTagName("parameter");
        Map<String, Map<String, Node>> parameterNodesByMacro = new HashMap<>();

        for (int i = 0; i < parameterNodes.getLength(); i++) {
            Node parameterNode = parameterNodes.item(i);
            String macroName = parameterNode.getParentNode().getParentNode().getAttributes().getNamedItem("name").getTextContent();
            String parameterName = parameterNode.getAttributes().getNamedItem("name").getTextContent();

            if (!parameterNodesByMacro.containsKey(macroName)) {
                parameterNodesByMacro.put(macroName, new HashMap<>());
            }

            parameterNodesByMacro.get(macroName).put(parameterName, parameterNode);
        }

        return parameterNodesByMacro;
    }

    private static String getAttributeValue(Node node, String attributeName) {
        Node attribute = node.getAttributes().getNamedItem(attributeName);
        return attribute == null ? null : attribute.getTextContent();
    }
}
