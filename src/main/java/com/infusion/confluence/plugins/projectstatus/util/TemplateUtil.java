package com.infusion.confluence.plugins.projectstatus.util;

import org.apache.commons.lang3.text.StrSubstitutor;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class TemplateUtil {
    public TemplateUtil() {
    }

    public static String pluralize(int count, String singular) {
        String plural = singular.concat("s");

        return count == 1 ? singular : plural;
    }

    public static String createGuid() {
        return UUID.randomUUID().toString();
    }

    public static String formatList(List<Map<String, String>> list, String elementFormat, String separator) {
        return list
                .stream()
                .map(element -> StrSubstitutor.replace(elementFormat, element))
                .collect(Collectors.joining(separator));
    }
}
