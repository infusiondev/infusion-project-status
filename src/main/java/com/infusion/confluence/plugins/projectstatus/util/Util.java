package com.infusion.confluence.plugins.projectstatus.util;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.infusion.confluence.plugins.projectstatus.aggregator.IssuesAggregationBuilder;
import com.infusion.confluence.plugins.projectstatus.aggregator.IssuesAggregator;
import com.infusion.confluence.plugins.projectstatus.data.JiraIssue;
import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Util {

    private static final String LIST_SEPARATOR_REGEX = "[,]";

    private Util() {}

    public static Object getStringValueByPath(final Map<String, Object> map, final String... path) {
        Map<?, ?> currentMap = map;

        for (int i = 0; i < path.length - 1; i++) {
            String pathElement = path[i];

            if (!(currentMap.get(pathElement) instanceof Map)) {
                return null;
            }

            currentMap = (Map<?, ?>) currentMap.get(pathElement);
        }

        String propertyPath = path[path.length - 1];

        return currentMap.get(propertyPath);
    }

    public static <K, V> V ensureEntry(Map<K, V> map, K key, Supplier<V> valueSupplier) {
        if (!map.containsKey(key)) {
            map.put(key, valueSupplier.get());
        }

        return map.get(key);
    }

    public static <K, V, R> R getOrDefault(Map<K, V> map, K key, R defaulValue, Function<V, R> valueParser) {
        if (map.containsKey(key)) {
            return valueParser.apply(map.get(key));
        }
        return defaulValue;
    }

    @SafeVarargs
    public static <T> T[] toArrayOrNull(List<T> list, T... array) {
        if (list == null)
            return null;
        return list.toArray(array);
    }

    public static <T> Stream<T> stream(Iterable<T> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false);
    }

    public static <T> T buildAggregate(IssuesAggregator<T> aggregator, Iterable<JiraIssue> issues) {
        IssuesAggregationBuilder<T> builder = aggregator.getAggregationBuilder();

        for(JiraIssue issue: issues) {
            builder.feed(issue);
        }

        return builder.build();
    }

    public static List<String> splitList(String input) {
        if (input == null) {
            return null;
        }

        List<String> result = new ArrayList<>();
        String[] elements = input.split(LIST_SEPARATOR_REGEX);

        for (String element : elements) {
            if (!Strings.isNullOrEmpty(element))
                result.add(element);
        }

        return result;
    }

    public static int calculatePercent(double total, double value) {
        return (int) Math.round(value / total * 100);
    }

    public static <K, V> Map<K, V> combineMaps(Map<K, V>... maps) {
        Map<K, V> combined = new HashMap<>();

        for (Map<K, V> map: maps) {
            combined.putAll(map);
        }

        return combined;
    }

    public static <K, V> Map<K, V> emptyMap() {
        return ImmutableMap.of();
    }

    public static String prepareUrlElement(Object queryStringElement) {
        try {
            // RFC 4627: "JSON text SHALL be encoded in Unicode. The default encoding is UTF-8."
            return URLEncoder.encode(String.valueOf(queryStringElement), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw ExceptionHelper.getUnspecifiedException(e);
        }
    }

    public static String prepareQueryString(Map<Object, Object> params) {
        return params
                .keySet()
                .stream()
                .map(key -> MessageFormat.format(
                                "{0}={1}",
                                prepareUrlElement(key),
                                prepareUrlElement(params.get(key))))
                .collect(Collectors.joining("&"));
    }

    public static URI getURI(String urlFormat, Object... params) {
        try {
            String url = params.length > 0 ?
                    MessageFormat.format(urlFormat, Stream.of(params).map(Util::prepareUrlElement).toArray()) :
                    urlFormat;

            return new URI(url);
        } catch (URISyntaxException e) {
            throw ExceptionHelper.getUnspecifiedException(e);
        }
    }

    public static URL getURL(String urlFormat, Object... params) {
        try {
            String url = params.length > 0 ?
                    MessageFormat.format(urlFormat, Stream.of(params).map(Util::prepareUrlElement).toArray()) :
                    urlFormat;

            return new URI(url).toURL();
        } catch (URISyntaxException|MalformedURLException e) {
            throw ExceptionHelper.getUnspecifiedException(e);
        }
    }

    public static String readFromURL(URL url) {
        HttpGet request;

        try {
            request = new HttpGet(url.toURI());
        } catch (URISyntaxException e) {
            throw ExceptionHelper.getMalformedUrlException();
        }

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            try(CloseableHttpResponse response = httpClient.execute(request)) {
                // have to override the default encoding as utf-8 is the default in IIS 7.5 (whereas it should be iso-8859-1 according to the specification)
                return EntityUtils.toString(response.getEntity(), Consts.UTF_8);
            }
        } catch (IOException e) {
            throw ExceptionHelper.getCommunicationException(e);
        }
    }

    public static String getSimpleMacroParameter(Map<String, String> macroParameters, String macroParameterName) {
        return decodeMacroParameterValue(macroParameters.get(macroParameterName));
    }

    public static List<String> getListMacroParameter(Map<String, String> macroParameters, String macroParameterName) {
        String rawParameterValue = macroParameters.get(macroParameterName);

        if (rawParameterValue.isEmpty()) {
            return new ArrayList<>();
        }

        return Stream
                .of(rawParameterValue.split(","))
                .map(Util::decodeMacroParameterValue)
                .filter(value -> value != null)
                .collect(Collectors.toList());
    }

    public static double secondsToHours(double seconds) {
        return seconds / (60 * 60);
    }

    private static String decodeMacroParameterValue(String rawParameterValue) {
        try {
            if (rawParameterValue.isEmpty()) {
                return null;
            }

            String[] parts = rawParameterValue.split("[|]");

            if (parts[0].isEmpty()) {
                return null;
            }

            return URLDecoder.decode(parts[0], "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw ExceptionHelper.getUnspecifiedException(e);
        }
    }
}
