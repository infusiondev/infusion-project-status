define(
    'infusion/infusion-project-status/common/async-util',
    [ 'jquery' ],
    function($) {
        // XHRs get converted to promises when piped.
        // This is why the resulting promise needs to be extended with abort() method (used by select2 control).
        var abortable = function(xhr) {
            if (!$.isFunction(xhr.abort)) {
                throw new Error('No abort method!');
            }

            return $.extend(xhr, {
                abortablePromise: function() {
                    return xhr.promise({
                        abort: xhr.abort
                    });
                }
             })
        };

        var pipeAbortablePromises = function(initialPromise) {
            var pipe = initialPromise;
            var current = initialPromise;

            for (var i = 1; i < arguments.length; i++) {
                var promiseProvider = arguments[i];
                var wrapper = function() {
                    return current = promiseProvider.apply(this, arguments);
                };

                pipe = pipe.pipe(wrapper);
            };

            return pipe.promise({
                abort: function() {
                    current && current.abort && current.abort();
                }
            });
        };

        var whenAbortablePromises = function() {
            return $
                .when
                .apply(this, arguments)
                .promise({
                    abort: function() {
                        $.each(arguments, function(abortablePromise) {
                            abortablePromise &&
                                abortablePromise.abort &&
                                abortablePromise.abort();
                        })
                    }
            });
        };

        return {
            abortable: abortable,
            pipe: pipeAbortablePromises,
            when: whenAbortablePromises
        }
    });