define(
    'infusion/infusion-project-status/common/controls/config',
    [],
    function() {
        return {
            DISABLE_PLACEHOLDERS: false,
            SUGGESTION_AJAX_DELAY_MILLIS: 500,
            MULTIVALUED_FIELD_SEPARATOR: ',',
            COMPLEX_VALUE_SEPARATOR: '|'
        };
    });