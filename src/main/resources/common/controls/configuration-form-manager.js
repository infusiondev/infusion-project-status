define(
    'infusion/infusion-project-status/common/controls/configuration-form-manager',
    [
        'ajs',
        'infusion/infusion-project-status/common/util',
        'infusion/infusion-project-status/common/controls/simple-fields',
        'infusion/infusion-project-status/common/controls/jira-issue-status-chooser-factory',
        'infusion/infusion-project-status/common/controls/jira-project-chooser-factory',
        'infusion/infusion-project-status/common/controls/jira-label-chooser-factory',
        'infusion/infusion-project-status/common/controls/jira-epic-chooser-factory2',
        'infusion/infusion-project-status/common/controls/jira-milestone-chooser-factory'
    ],
    function(
        AJS,
        Util,
        simpleFields,
        issueStatusChooserFactory,
        projectChooserFactory,
        labelChooserFactory,
        epicChooserFactory,
        milestoneChooserFactory) {

        var PROJECT_DEPENDANT_FIELDS = ['milestoneDetailsView', 'epicDetailsView'];

        var SELECT_PROJECT_PLACEHOLDER = AJS.I18n.getText('infusion-project-status.common.controls.null-project-placeholder');

        var FIELD_CONFIG = $.extend(true, simpleFields.build(), {
            'string': {
                excludedStatuses: issueStatusChooserFactory.createControl,
                jiraProject: projectChooserFactory.createControl,
                includedLabels: labelChooserFactory.createControl,
                epicDetailsView: epicChooserFactory.createControl,
                milestoneDetailsView: milestoneChooserFactory.createControl
            }
        });

        var configureFields = function(macroName, fields) {
            var callIfFieldsExist = function(fieldNames, func) {
                return fieldNames
                    .filter(function(fieldName) {
                        return $.inArray(fieldName, fields) != -1;
                    })
                    .map(function(fieldName) {
                        return func(AJS.MacroBrowser.fields[fieldName]);
                    });
            };

            var updateProjectDependantFields = function(projectFieldValue, isInit) {
                var projectKey = projectFieldValue && Util.decodePropertyValue(projectFieldValue);

                callIfFieldsExist(PROJECT_DEPENDANT_FIELDS, function(field) {
                    return isInit ? field.initProjectId(projectKey) : field.setProjectId(projectKey);
                });
            };

            var projectChangedHandler = function(val) {
                AJS.log('Project changed: %s', JSON.stringify(val));
                updateProjectDependantFields(val);
            };

            var initForm = function(selectedParams, selectedMacro) {
                updateProjectDependantFields(selectedParams.jiraProject, true);

                callIfFieldsExist(['jiraProject'], function(field) {
                    field.onChanged(projectChangedHandler);
                });

                // initial field values can be overriden here
                return selectedParams;
            };

            return AJS.MacroBrowser.setMacroJsOverride(macroName, {
                fields: FIELD_CONFIG,
                beforeParamsSet: initForm
            });
        };

        return {
            configureFields: configureFields
        };
    });