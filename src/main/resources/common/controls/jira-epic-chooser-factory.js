define('infusion/infusion-project-status/common/controls/jira-epic-chooser-factory',
    ['ajs', 'infusion/infusion-project-status/common/controls/select-control-factory-provider'],
    function(AJS, SelectControlFactoryProvider) {
        var getId = function(epic) {
            return epic.id;
        };

        var mapSuggestionToEntity = function(suggestion) {
            return {
                id: suggestion.value,
                searchHintHtml: suggestion.displayName
            };
        };

        var restoreObjectsFromIds = function(ids) {
            return ids.map(function(id) {
                return {
                    id: id,
                    searchHintHtml: ''
                }
            });
        };

        return SelectControlFactoryProvider.provide({
            displayName: AJS.I18n.getText('infusion-project-status.common.controls.jira-epic-chooser.label'),
            displayDescription: AJS.I18n.getText('infusion-project-status.common.controls.jira-epic-chooser.desc'),
            placeholder: AJS.I18n.getText('infusion-project-status.common.controls.jira-epic-chooser.placeholder'),
            multiple: true,
            getId: getId,
            getTextForSearchResult: function(epic) { return epic.searchHintHtml; },
            getTextForSelection: getId,
            suggestionFieldName: 'Epic Link',
            mapSuggestionToEntity: mapSuggestionToEntity,
            restoreObjectsFromIds: restoreObjectsFromIds
        });
    });
