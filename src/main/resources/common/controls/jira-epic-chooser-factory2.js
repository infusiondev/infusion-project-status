define('infusion/infusion-project-status/common/controls/jira-epic-chooser-factory2',
    [
        'ajs',
        'infusion/infusion-project-status/common/controls/select-control-factory-provider',
        'infusion/infusion-project-status/common/util',
        'infusion/infusion-project-status/common/controls/project-dependant-control-state-factory',
        'infusion/infusion-project-status/common/controls/project-dependant-control-api-factory',
        'infusion/infusion-project-status/common/jira-connector'
    ],
    function(AJS, SelectControlFactoryProvider, Util, StateFactory, ApiFactory, JiraConnector) {
        var REST_ENDPOINT = '/rest/api/2/search';

        var factoryOptionsProvider = function() {
            // this state object sticks to every control instance
            var state = StateFactory.get();

            var transport = function(ajaxOptions) {
                if (!ajaxOptions.data.projectId) {
                    return ajaxOptions.success([]);
                }

                var jql = AJS.format(
                      'project = {0} AND issuetype = Epic' + (ajaxOptions.data.searchTerm ? ' AND text ~ "{1}"' : ''),
                      ajaxOptions.data.projectId,
                      ajaxOptions.data.searchTerm)

                var transportOptions = {
                    url: ajaxOptions.url,
                    data: {
                        queryParams: {
                            jql: jql,
                            fields: 'summary'
                        }
                    },
                    success: function(data) {
                        return ajaxOptions.success(data.issues);
                    }
                };

                return JiraConnector.jiraTransport(transportOptions);
            };

            var getTextForSearchResult = function(milestone) {
                return milestone.searchHintHtml;
            };

            var mapDto = function(dto) {
                 return {
                     id: dto.key,
                     searchHintHtml: dto.fields.summary
                 };
            };

            var getId = function(milestone) {
                return milestone.id;
            };

            var restoreObjectsFromIds = function(ids) {
                return ids.map(function(id) {
                    return { id: id }
                });
            };

            var getAjaxData = function(searchTerm) {
                return {
                    projectId: state.getProjectId(),
                    searchTerm: searchTerm
                }
            };

            var getResults = function(data, page) {
                return {
                    results: data.map(mapDto)
                }
            };

            return {
                displayName: AJS.I18n.getText('infusion-project-status.common.controls.jira-epic-chooser.label'),
                displayDescription: AJS.I18n.getText('infusion-project-status.common.controls.jira-epic-chooser.desc'),
                placeholder: AJS.I18n.getText('infusion-project-status.common.controls.jira-epic-chooser.placeholder'),
                multiple: true,
                getId: getId,
                getTextForSearchResult: getTextForSearchResult,
                getTextForSelection: getId,
                restoreObjectsFromIds: restoreObjectsFromIds,
                extendApi: ApiFactory.get.bind(this, state),
                ajax: {
                    url: REST_ENDPOINT,
                    data: getAjaxData,
                    results: getResults,
                    transport: transport
                }
            };
        };

        return SelectControlFactoryProvider.provide(factoryOptionsProvider);
    });
