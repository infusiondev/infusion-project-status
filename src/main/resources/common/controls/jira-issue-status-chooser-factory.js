define('infusion/infusion-project-status/common/controls/jira-issue-status-chooser-factory',
    [
        'ajs',
        'infusion/infusion-project-status/common/controls/select-control-factory-provider',
        'infusion/infusion-project-status/common/util',
        'infusion/infusion-project-status/common/jira-connector',
        'infusion/infusion-project-status/common/async-util'
    ],
    function(AJS, SelectControlFactoryProvider, Util, JiraConnector, AsyncUtil) {
        var STATUS_REST_ENDPOINT = '/rest/api/2/status';
        var MACRO_CSS_CLASS = 'project-status';
        var CATEGORY_ORDER = {
            'new': 10,
            'indeterminate': 20,
            'done': 30,
            'undefined': 40
        };

        var optionProvider = function () {
            // this state object sticks to every control instance
            var state = (function() {
                var _cache;

                return {
                    setCache: function(data) {
                        _cache = data;
                    },
                    getCache: function() {
                        return _cache;
                    },
                    hasCache: function() {
                        return _cache;
                    },
                };
            })();

            var formatChoiceCssClass = function(option) {
                return AJS.format(
                   'status-category-block-{0} status-category-color-{1}',
                   option.statusCategoryKey,
                   option.statusCategoryColorName);
            };

            var enrichSuggestions = function(suggestions) {
                var extendedStatusInfoPromise = state.hasCache() &&
                    $.Deferred().resolve(state.getCache()) ||
                    JiraConnector.queryJira({
                        restPath: STATUS_REST_ENDPOINT
                    });

                var enrich = function(statusExtendedInfo) {
                    state.setCache(statusExtendedInfo);

                    var statusIndex = statusExtendedInfo
                        .reduce(function(o, status) {
                            o[status.name] = status;
                            return o;
                        }, {});

                    return suggestions
                        .filter(function(suggestion) {
                            return statusIndex[Util.stripHtml(suggestion.displayName)];
                        })
                        .map(function(suggestion) {
                            return $.extend(suggestion, {
                                statusCategory: statusIndex[Util.stripHtml(suggestion.displayName)].statusCategory
                            });
                        });
                };

                return AsyncUtil.pipe(extendedStatusInfoPromise, enrich);
            };

            var mapSuggestionToEntity = function(suggestion) {
                return {
                    id: suggestion.value,
                    searchHintHtml: suggestion.displayName,
                    statusCategoryKey: suggestion.statusCategory.key,
                    statusCategoryColorName: suggestion.statusCategory.colorName
                };
            };

            var getId = function(status) {
                return [
                    status.id,
                    Util.stripHtml(status.searchHintHtml)
                ];
            };

            var getSearchHintHtml = function(status) {
                return status.searchHintHtml;
            };

            var restoreObjectsFromIds = function(ids) {
                var restoredSuggestions = ids
                    .map(function(propertiesArray) {
                        return {
                            value: propertiesArray[0],
                            displayName: propertiesArray[1]
                        };
                    });

                return AsyncUtil.pipe(
                    enrichSuggestions(restoredSuggestions),
                    function(enrichedSuggestions) {
                        return enrichedSuggestions.map(mapSuggestionToEntity);
                    }
                );
            };

            var sortResults = function (results, container, query) {
                return results.sort(function(dataElement1, dataElement2) {
                    return CATEGORY_ORDER[dataElement1.statusCategoryKey] - CATEGORY_ORDER[dataElement2.statusCategoryKey];
                });
            };

            var enrichAjaxResult = function(data) {
                return AsyncUtil.pipe(
                    enrichSuggestions(data.results),
                    function(results) {
                        data.results = results;
                        return data;
                    });
            };

            return {
                displayName: AJS.I18n.getText('infusion-project-status.common.controls.jira-issue-status-chooser.label'),
                displayDescription: AJS.I18n.getText('infusion-project-status.common.controls.jira-issue-status-chooser.desc'),
                placeholder: AJS.I18n.getText('infusion-project-status.common.controls.jira-issue-status-chooser.placeholder'),
                multiple: true,
                getId: getId,
                getTextForSearchResult: getSearchHintHtml,
                getTextForSelection: getSearchHintHtml,
                restoreObjectsFromIds: restoreObjectsFromIds,
                suggestionFieldName: 'status',
                mapSuggestionToEntity: mapSuggestionToEntity,
                sortResults: sortResults,
                enrichAjaxResult: enrichAjaxResult,
                style: {
                    containerCssClass: MACRO_CSS_CLASS,
                    dropdownCssClass: MACRO_CSS_CLASS,
                    formatSelectionCssClass: formatChoiceCssClass,
                    formatResultCssClass: formatChoiceCssClass
                }
            };
        };

        return SelectControlFactoryProvider.provide(optionProvider);
    });
