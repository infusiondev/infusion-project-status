define('infusion/infusion-project-status/common/controls/jira-label-chooser-factory',
    [
        'ajs',
        'infusion/infusion-project-status/common/controls/select-control-factory-provider',
        'infusion/infusion-project-status/common/util'
    ],
    function(AJS, SelectControlFactoryProvider, Util) {
        var getId = function(label) {
            return [
                label.id,
                Util.stripHtml(label.searchHintHtml)
            ];
        };

        var mapSuggestionToEntity = function(suggestion) {
            return {
                id: suggestion.value,
                searchHintHtml: suggestion.displayName
            }
        };

        var restoreObjectsFromIds = function(ids) {
            return ids.map(function(propertyArray) {
                return {
                    id: propertyArray[0],
                    searchHintHtml: propertyArray[1]
                }
            });
        };

        return SelectControlFactoryProvider.provide({
            displayName: AJS.I18n.getText('infusion-project-status.common.controls.jira-label-chooser.label'),
            displayDescription: AJS.I18n.getText('infusion-project-status.common.controls.jira-label-chooser.desc'),
            placeholder: AJS.I18n.getText('infusion-project-status.common.controls.jira-label-chooser.placeholder'),
            multiple: true,
            getId: getId,
            getTextForSearchResult: function(label) { return label.searchHintHtml; },
            getTextForSelection: function(label) { return label.searchHintHtml; },
            suggestionFieldName: 'Labels',
            mapSuggestionToEntity: mapSuggestionToEntity,
            restoreObjectsFromIds: restoreObjectsFromIds
        });
    });
