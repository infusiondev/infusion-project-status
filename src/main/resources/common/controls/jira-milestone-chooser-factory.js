define('infusion/infusion-project-status/common/controls/jira-milestone-chooser-factory',
    [
        'ajs',
        'infusion/infusion-project-status/common/controls/select-control-factory-provider',
        'infusion/infusion-project-status/common/util',
        'infusion/infusion-project-status/common/controls/project-dependant-control-state-factory',
        'infusion/infusion-project-status/common/controls/project-dependant-control-api-factory'
    ],
    function(AJS, SelectControlFactoryProvider, Util, StateFactory, ApiFactory) {
        var REST_ENDPOINT = Util.makeConfluenceUrl('/rest/project-status/1.0/macro-editor/suggestions/milestones');

        var factoryOptionsProvider = function() {
            // this state object sticks to every control instance
            var state = StateFactory.get();

            var transport = function(ajaxOptions) {
                var projectId = ajaxOptions.data.projectId;
                var searchTerm = ajaxOptions.data.searchTerm;
                var transport = $.fn.select2.ajaxDefaults.transport;
                var success = ajaxOptions.success;

                if (!projectId) {
                    return success([]);
                }

                // don't need the search term anymore -> isn't sent to the service anyway
                delete ajaxOptions.data.searchTerm;

                ajaxOptions.success = function(data) {
                    state.setCache(projectId, data);

                    var filteredData = data.filter(function(element) {
                        return getTextForSearchResult(mapDto(element))
                            .toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
                    });

                    return success(filteredData);
                };

                if (state.hasCache(projectId)) {
                    return ajaxOptions.success(state.getCache(projectId));
                }

                return transport.apply(this, arguments);
            };

            var getTextForSearchResult = function(milestone) {
                return milestone.searchHintHtml;
            };

            var mapDto = function(dto) {
                 return {
                     id: dto,
                     searchHintHtml: dto
                 };
            };

            var getId = function(milestone) {
                return milestone.id;
            };

            var restoreObjectsFromIds = function(ids) {
                return ids.map(function(id) {
                    return { id: id }
                });
            };

            var getAjaxData = function(searchTerm) {
                return {
                    projectId: state.getProjectId(),
                    searchTerm: searchTerm
                }
            };

            var getResults = function(data, page) {
                return {
                    results: data.map(mapDto)
                }
            };

            return {
                displayName: AJS.I18n.getText('infusion-project-status.common.controls.jira-milestone-chooser.label'),
                displayDescription: AJS.I18n.getText('infusion-project-status.common.controls.jira-milestone-chooser.desc'),
                placeholder: AJS.I18n.getText('infusion-project-status.common.controls.jira-milestone-chooser.placeholder'),
                multiple: true,
                getId: getId,
                getTextForSearchResult: getTextForSearchResult,
                getTextForSelection: getId,
                restoreObjectsFromIds: restoreObjectsFromIds,
                extendApi: ApiFactory.get.bind(this, state),
                ajax: {
                    url: REST_ENDPOINT,
                    data: getAjaxData,
                    results: getResults,
                    transport: transport
                }
            };
        };

        return SelectControlFactoryProvider.provide(factoryOptionsProvider);
    });
