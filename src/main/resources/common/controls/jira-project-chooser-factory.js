define('infusion/infusion-project-status/common/controls/jira-project-chooser-factory',
    [
        'ajs',
        'infusion/infusion-project-status/common/controls/select-control-factory-provider',
        'infusion/infusion-project-status/common/util'
    ],
    function(AJS, SelectControlFactoryProvider, Util) {
        var getProjectIdFromSuggestionDisplayName = function(displayName) {
            var strippedDisplayName = Util.stripHtml(displayName);

            var index1 = strippedDisplayName.lastIndexOf('(');
            var index2 = strippedDisplayName.lastIndexOf(')');

            return strippedDisplayName.substring(index1 + 1, index2);
        };

        var apiDtoToProject = function(dto) {
            return {
                id: dto.key,
                name: dto.name,
                searchHintHtml: ''
            }
        };

        var mapSuggestionToEntity = function(suggestion) {
            return {
                id: getProjectIdFromSuggestionDisplayName(suggestion.displayName),
                searchHintHtml: suggestion.displayName
            };
        };

        var restoreObjectsFromIds = function(id) {
            return {
                id: id[0],
                searchHintHtml: id[1]
            };
        };

        return SelectControlFactoryProvider.provide({
            displayName: AJS.I18n.getText('infusion-project-status.common.controls.jira-project-chooser.label'),
            displayDescription: AJS.I18n.getText('infusion-project-status.common.controls.jira-project-chooser.desc'),
            placeholder: AJS.I18n.getText('infusion-project-status.common.controls.jira-project-chooser.placeholder'),
            multiple: false,
            getId: function(project) { return [ project.id, project.searchHintHtml ]; },
            getTextForSearchResult: function(project) { return project.searchHintHtml; },
            getTextForSelection: function(project) { return project.searchHintHtml; },
            suggestionFieldName: 'project',
            mapSuggestionToEntity: mapSuggestionToEntity,
            restoreObjectsFromIds: restoreObjectsFromIds
        });
    });

