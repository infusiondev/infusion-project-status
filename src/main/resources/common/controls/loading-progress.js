define(
    'infusion/infusion-project-status/common/controls/loading-progress',
    [ 'ajs' ],
    function(AJS) {
        var loadingCounter = 0;

        var loadingInProgress = function(isLoading) {
            if (isLoading) {
                AJS.MacroBrowser.dialog.disable();
            } else {
                AJS.MacroBrowser.dialog.enable();
            }

            AJS.MacroBrowser.UI.showBrowserSpinner(isLoading);
        };

        var startLoading = function() {
            loadingCounter++;
            loadingInProgress(true);
        };

        var stopLoading = function() {
            if (--loadingCounter == 0) {
                loadingInProgress(false);
            }
        };

        return {
            startLoading: startLoading,
            stopLoading: stopLoading
        }
    });