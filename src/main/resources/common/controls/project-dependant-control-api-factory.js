define('infusion/infusion-project-status/common/controls/project-dependant-control-api-factory', [],
    function() {
        var get = function(state, field) {
            var CHOOSE_PROJECT_PLACEHOLDER = AJS.I18n.getText('infusion-project-status.common.controls.null-project-placeholder');

            var setProjectId = function(field, projectId) {
                if (!projectId) {
                    field.disableWithPlaceholder(CHOOSE_PROJECT_PLACEHOLDER);
                } else if (field.isDisabledWithPlaceholder()) {
                    field.initControl();
                }

                state.setProjectId(projectId);
            };

            return {
                initProjectId: function(projectId) {
                    setProjectId(field, projectId);
                },
                setProjectId: function(projectId) {
                    if (state.compareProjectId(projectId)) {
                        return;
                    }

                    field.clearValue();
                    setProjectId(field, projectId);
                }
            };
        };

        return {
            get: get
        };
    });
