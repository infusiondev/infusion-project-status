define('infusion/infusion-project-status/common/controls/project-dependant-control-state-factory', [],
    function() {
        var get = function() {
            var _cache = {};
            var _projectId;

            var clearCache = function() {
                _cache = {};
            };

            var setCache = function(projectId, data) {
                clearCache();
                _cache[projectId] = data;
            };

            var hasCache = function(projectId) {
                return _cache && _cache[projectId];
            };

            var getCache = function(projectId) {
                return _cache[projectId];
            };

            var compareProjectId = function(projectId) {
                return projectId === _projectId;
            };

            var setProjectId = function(projectId) {
                if (!compareProjectId(projectId)) {
                    clearCache();
                }

                _projectId = projectId;
            };

            var getProjectId = function(projectId) {
                return _projectId;
            };

            return {
                setCache: setCache,
                hasCache: hasCache,
                getCache: getCache,
                compareProjectId: compareProjectId,
                setProjectId: setProjectId,
                getProjectId: getProjectId
            };
        };

        return {
            get: get
        };
    });
