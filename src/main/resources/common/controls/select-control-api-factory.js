define('infusion/infusion-project-status/common/controls/select-control-api-factory', [],
    function() {
        var get = function(field, options, auiSelectOptions) {
            var isDisabledWithPlaceholderFlag = false;

            var workAroundInitialSelectionBug = function() {
                field.input.data('select2').disable();

                setTimeout(function() {
                    field.input.change();
                    field.input.data('select2').enable();
                });
            };

            var setValue = function(val) {
                field.input.val(val);

                if (isDisabledWithPlaceholderFlag) {
                    return;
                }

                if (options.multiple) {
                    // multiple
                    workAroundInitialSelectionBug();
                    return;
                }

                // single
                field.input.change();
            };

            var onChanged = function(callback) {
                field.input.on(
                    options.multiple ? 'selected' : 'select2-selected',
                    function(event) {
                        callback(event.val)
                    });
            };

            var disableWithPlaceholder = function(placeholder) {
                if (isDisabledWithPlaceholderFlag) {
                    return;
                }

                field.input.data('select2') && field.input.data('select2').destroy();
                field.input.attr('disabled', true);
                field.input.attr('placeholder', placeholder);
                isDisabledWithPlaceholderFlag = true;
            };

            var isDisabledWithPlaceholder = function() {
                return isDisabledWithPlaceholderFlag;
            };

            var initControl = function() {
                if (isDisabledWithPlaceholderFlag) {
                    field.input.attr('disabled', false);
                }

                field.input.attr('placeholder', options.placeholder);
                field.input.auiSelect2(auiSelectOptions);
            };

            var clearValue = function() {
                field.input.data('select2') && field.input.data('select2').val(null);
            };

            return {
                onChanged: onChanged,
                disableWithPlaceholder: disableWithPlaceholder,
                isDisabledWithPlaceholder: isDisabledWithPlaceholder,
                initControl: initControl,
                clearValue: clearValue,
                setValue: setValue
            };
        };

        return {
            get: get
        };
    });
