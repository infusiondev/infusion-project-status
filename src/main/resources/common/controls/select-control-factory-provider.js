define('infusion/infusion-project-status/common/controls/select-control-factory-provider',
    [
        'underscore',
        'jquery',
        'confluence',
        'ajs',
        'infusion/infusion-project-status/common/util',
        'infusion/infusion-project-status/common/controls/config',
        'infusion/infusion-project-status/common/controls/loading-progress',
        'infusion/infusion-project-status/common/jira-connector',
        'infusion/infusion-project-status/common/async-util',
        'infusion/infusion-project-status/common/controls/select-control-api-factory'
    ],
    function(_, $, Confluence, AJS, Util, Config, LoadingProgress, JiraConnector, AsyncUtil, ApiFactory) {
        var createControl = function(optionsProvider, param) {
            // private fields
            var options = $.isFunction(optionsProvider) ? optionsProvider() : optionsProvider;
            var SUGGESTIONS_REST_URL = '/rest/api/2/jql/autocompletedata/suggestions';

            var ajaxData = function(searchTerm) {
                return {
                    queryParams: {
                        fieldName: options.suggestionFieldName,
                        fieldValue: searchTerm || ''
                    }
                }
            };

            var ajaxResults = function(data, page) {
                return {
                    results: data.results.map(options.mapSuggestionToEntity)
                }
            };

            var ajaxTransport = function(ajaxParams) {
                var transport = JiraConnector.jiraTransport;
                var success = ajaxParams.success;

                delete ajaxParams.success;

                var promise = transport(ajaxParams);

                if (options.enrichAjaxResult) {
                    promise = AsyncUtil.pipe(
                        promise,
                        options.enrichAjaxResult
                    );
                }

                promise.done(success);
            };

            var getAjax = function() {
                return {
                    quietMillis: Config.SUGGESTION_AJAX_DELAY_MILLIS,
                    url: SUGGESTIONS_REST_URL,
                    data: ajaxData,
                    results: ajaxResults,
                    transport: ajaxTransport
                };
            };

            var initSelection = function(element, callback) {
                if (!element.val()) {
                    return;
                }

                var decodedIds = Util.decodeFieldValue(element.val(), options.multiple);
                var restoreObjectsResult = options.restoreObjectsFromIds(decodedIds);

                if (!$.isFunction(restoreObjectsResult.promise)) {
                    return callback(restoreObjectsResult);
                }

                LoadingProgress.startLoading();

                return restoreObjectsResult
                    .always(LoadingProgress.stopLoading)
                    .done(function(restoredObjects) {
                        callback(restoredObjects);
                    });
            };

            var formatResult = function (entity, container, query, escapeMarkup) {
                arguments[0] = { text: options.getTextForSearchResult(entity) };
                arguments[3] = Util.doNotEscapeMarkup;
                return $.fn.select2.defaults.formatResult.apply(this, arguments);
            };

            var formatSelection = function (entity, container, escapeMarkup) {
                arguments[0] = { text: options.getTextForSelection(entity) };
                arguments[2] = Util.stripHtml;
                return $.fn.select2.defaults.formatSelection.apply(this, arguments);
            };

            var id = function(element) {
                var getId = options.getId || $.fn.select2.defaults.id;
                return Util.encodeFieldValue(getId(element));
            };

            var construct = function() {
                var field = AJS.MacroBrowser.ParameterFields['string']($.extend(param, {
                    displayName: options.displayName,
                    description: options.displayDescription
                }));

                if (Config.DISABLE_PLACEHOLDERS) {
                    delete options.placeholder;
                }

                var auiSelectOptions = {
                    separator: Config.MULTIVALUED_FIELD_SEPARATOR,
                    containerCssClass: options.style && options.style.containerCssClass,
                    dropdownCssClass: options.style && options.style.dropdownCssClass,
                    formatSelectionCssClass: options.style && options.style.formatSelectionCssClass,
                    formatResultCssClass: options.style && options.style.formatResultCssClass,
                    multiple: options.multiple,
                    initSelection: initSelection,
                    sortResults: options.sortResults || $.fn.select2.defaults.sortResults,
                    formatResult: formatResult,
                    formatSelection: formatSelection,
                    id: id,
                    ajax: $.extend(getAjax(), options.ajax || {})
                }

                field = $.extend(field, ApiFactory.get(field, options, auiSelectOptions));
                field.initControl();

                return $.extend(field, (options.extendApi || $.noop)(field));
            };

            return construct();
        };

        return {
            provide: function(optionsProvider) {
                return {
                    createControl: createControl.bind(this, optionsProvider)
                }
            }
        }
    });
