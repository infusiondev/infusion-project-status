define(
    'infusion/infusion-project-status/common/controls/simple-fields',
    [ 'ajs' ],
    function(AJS) {
        var parameterFieldWithStringsOverride = function(type, displayName, displayDescription) {
            return function(params) {
                params.displayName = displayName;
                params.description = displayDescription;
                return AJS.MacroBrowser.ParameterFields[type](params);
            };
        };

        return {
            build: function() {
                return {
                    'string': {
                        width: parameterFieldWithStringsOverride(
                            'string',
                            AJS.I18n.getText('infusion-project-status.common.controls.width-input.label'),
                            AJS.I18n.getText('infusion-project-status.common.controls.width-input.desc')),
                        epicCaption: parameterFieldWithStringsOverride(
                            'string',
                            AJS.I18n.getText('infusion-project-status.common.controls.epic-caption-input.label'),
                            AJS.I18n.getText('infusion-project-status.common.controls.epic-caption-input.desc')),
                    },
                    'boolean': {
                        border: parameterFieldWithStringsOverride(
                            'boolean',
                             AJS.I18n.getText('infusion-project-status.common.controls.border-input.label'),
                             AJS.I18n.getText('infusion-project-status.common.controls.border-input.desc')),
                        showHours: parameterFieldWithStringsOverride(
                            'boolean',
                             AJS.I18n.getText('infusion-project-status.common.controls.show-hours.label'),
                             AJS.I18n.getText('infusion-project-status.common.controls.show-hours.desc'))
                    }
                }
            }
        }
    });