define(
    'infusion/infusion-project-status/common/jira-connector',
    [
        'jquery',
        'ajs',
        'infusion/infusion-project-status/common/util',
        'infusion/infusion-project-status/common/url-util',
        'infusion/infusion-project-status/common/async-util'
    ],
    function($, AJS, Util, UrlUtil, AsyncUtil) {
        var jiraApplicationLink;
        var jiraApplicationLinkPath = Util.makeConfluenceUrl('/rest/project-status/1.0/jira-connector/application-link');
        var jiraProxyRest = Util.makeConfluenceUrl('/plugins/servlet/applinks/proxy');

        var queryJiraWithApplicationLinkId = function(jiraApplicationLinkId, options) {
            var jiraPath = options.restPath;

            if (options.pathParams) {
                jiraPath += UrlUtil.preparePathParamsString(options.pathParams);
            }

            if (options.queryParams) {
                jiraPath += AJS.format('?{0}', UrlUtil.prepareQueryString(options.queryParams));
            }

            return $
                .getJSON(
                    jiraProxyRest, {
                        appId: jiraApplicationLinkId,
                        path: jiraPath
                    });
        };

        var queryJira = function(options) {
            return AsyncUtil.pipe(
                getJiraApplicationLinkId(),
                function(appLinkId) {
                    return AsyncUtil.abortable(queryJiraWithApplicationLinkId(appLinkId, options))
                        .abortablePromise();
                });
        };

        var getJiraApplicationLinkId = function() {
            if (jiraApplicationLink) {
                return $.Deferred().resolve(jiraApplicationLink.id);
            }

            var getJiraApplicationLinkPromise =
                AsyncUtil.abortable($.getJSON(jiraApplicationLinkPath)).abortablePromise();

            return AsyncUtil.pipe(
                getJiraApplicationLinkPromise,
                function(result) {
                    jiraApplicationLink = result;
                    return result.id;
                });
        };

        var jiraTransport = function(params) {
            var result = queryJira({
                restPath: params.url,
                queryParams: params.data.queryParams,
                pathParams: params.data.pathParams
            });

            if (params.success) {
                result = result.done(params.success);
            }

            return result;
        };

        return {
            queryJira: queryJira,
            jiraTransport: jiraTransport,
        }
    });