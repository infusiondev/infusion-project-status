define(
    'infusion/infusion-project-status/common/url-util',
    [ 'ajs' ],
    function(AJS) {
        var prepareQueryString = function(queryParams) {
            var queryStringParts = [];

            for (var key in queryParams) {
                queryStringParts.push(
                    AJS.format('{0}={1}',
                    encodeURIComponent(key),
                    encodeURIComponent(queryParams[key])))
            }

            return AJS.format('{0}', queryStringParts.join('&'));
        };

        var preparePathParamsString = function(pathParams) {
            var pathParamsString = '';

            pathParams.forEach(function(pathParam) {
                pathParamsString += AJS.format('/{0}', encodeURIComponent(pathParam));
            });

            return pathParamsString;
        };

        return {
            prepareQueryString: prepareQueryString,
            preparePathParamsString: preparePathParamsString
        }
    });