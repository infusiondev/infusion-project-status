define(
    'infusion/infusion-project-status/common/util',
    [ 'underscore', 'jquery', 'ajs', 'infusion/infusion-project-status/common/controls/config' ],
    function(_, $, AJS, Config) {
        var makeConfluenceUrl = function(url) {
            return AJS.format('{0}{1}', Confluence.getContextPath(), url);
        };

        var trimQuotes = function(str) {
            return str.replace(/(^("+))|(("+)$)/g, '');
        };

        var encodeFieldValue = function(value) {
            return $.isArray(value) ?
                value.map(encodeURIComponent).join(Config.COMPLEX_VALUE_SEPARATOR) :
                encodeURIComponent(value);
        };

        var decodeFieldValue = function(rawFieldValue, isMultiple) {
            var encodedValuesArray = isMultiple ?
                rawFieldValue.split(Config.MULTIVALUED_FIELD_SEPARATOR) :
                [ rawFieldValue ];

            var decodedValuesArray = encodedValuesArray
                .map(function(part) {
                    return part
                        .split(Config.COMPLEX_VALUE_SEPARATOR)
                        .map(decodeURIComponent);
                })
                .map(function(valuesArray) {
                    return valuesArray.length === 1 ?
                        valuesArray[0] :
                        valuesArray;
                });

            return isMultiple ? decodedValuesArray : decodedValuesArray[0];
        };

        var decodePropertyValue = function(rawFieldValue) {
            return decodeFieldValue(rawFieldValue)[0];
        };

        var stripHtml = function(html) {
            return $('<div />').html(html).text();
        };

        var doNotEscapeMarkup = function(markup) {
            return markup;
        };

        return {
            makeConfluenceUrl: makeConfluenceUrl,
            trimQuotes: trimQuotes,
            stripHtml: stripHtml,
            doNotEscapeMarkup: doNotEscapeMarkup,
            encodeFieldValue: encodeFieldValue,
            decodeFieldValue: decodeFieldValue,
            decodePropertyValue: decodePropertyValue
        }
    });