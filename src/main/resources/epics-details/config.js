require(['ajs', 'infusion/infusion-project-status/common/controls/configuration-form-manager'],
function(AJS, ConfigurationFormManager) {
    'use strict';

    AJS.log('Configuring Epics Details...');

    ConfigurationFormManager.configureFields("epics-details",
        ['jiraProject', 'excludedStatuses', 'includedLabels', 'epicDetailsView', 'showHours']);
});