require(['ajs', 'infusion/infusion-project-status/common/controls/configuration-form-manager'],
function(AJS, ConfigurationFormManager) {
    'use strict';

    AJS.log('Configuring Epics Summary...');

    ConfigurationFormManager.configureFields('epics-summary', ['jiraProject', 'excludedStatuses', 'includedLabels', 'epicDetailsView']);
});