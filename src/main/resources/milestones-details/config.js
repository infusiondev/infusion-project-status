require(['ajs', 'infusion/infusion-project-status/common/controls/configuration-form-manager'],
function(AJS, ConfigurationFormManager) {
    'use strict';

    AJS.log('Configuring Milestones Details...');

    ConfigurationFormManager.configureFields("milestones-details",
        ['jiraProject', 'excludedStatuses', 'includedLabels', 'milestoneDetailsView', 'showHours']);
});