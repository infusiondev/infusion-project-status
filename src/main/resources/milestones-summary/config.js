require(['ajs', 'infusion/infusion-project-status/common/controls/configuration-form-manager'],
function(AJS, ConfigurationFormManager) {
    'use strict';

    AJS.log('Configuring Milestones Summary...');

    ConfigurationFormManager.configureFields('milestones-summary', ['jiraProject', 'excludedStatuses', 'includedLabels', 'milestoneDetailsView']);
});