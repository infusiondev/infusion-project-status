require(['ajs', 'infusion/infusion-project-status/common/controls/configuration-form-manager'],
function(AJS, ConfigurationFormManager) {
    'use strict';

    AJS.log('Configuring Project Summary...');

    ConfigurationFormManager.configureFields('project-summary', ['jiraProject', 'excludedStatuses', 'includedLabels']);
});