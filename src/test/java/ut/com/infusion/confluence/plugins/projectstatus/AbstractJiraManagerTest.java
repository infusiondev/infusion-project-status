package ut.com.infusion.confluence.plugins.projectstatus;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class AbstractJiraManagerTest {
    protected static final String PROJECT_NAME = "TP";
    protected static final String FIELD_NAME_EPIC_NAME = "Epic Name";
    protected static final String FIELD_NAME_EPIC_LINK = "Epic Link";
    protected static final String ISSUE_TYPE_EPIC = "Epic";
    protected static final String ISSUE_TYPE_TASK = "Task";
    protected static final String EPIC_NAME_FIELD_ID = "customfield_10004";
    protected static final String EPIC_NAME_TECHNICAL_DEBT = "Technical Debt";
    protected static final String EPIC_NAME_TESTS_ENHANCEMENT = "Tests enhancement";

    protected String readFileToString(String filePath) throws IOException, URISyntaxException {
        URL fileUrl = this.getClass().getResource(filePath);
        return FileUtils.readFileToString(new File(fileUrl.toURI()));
    }
}
