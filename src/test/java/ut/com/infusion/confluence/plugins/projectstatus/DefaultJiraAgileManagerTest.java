package ut.com.infusion.confluence.plugins.projectstatus;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import com.infusion.confluence.plugins.projectstatus.DefaultJiraAgileManager;
import com.infusion.confluence.plugins.projectstatus.data.agile.RapidView;
import com.infusion.confluence.plugins.projectstatus.data.agile.SprintCalendarEntry;
import com.infusion.confluence.plugins.projectstatus.data.agile.SprintFromBoard;
import com.infusion.confluence.plugins.projectstatus.services.JiraConnectorService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@SuppressWarnings("unchecked")
public class DefaultJiraAgileManagerTest extends AbstractJiraManagerTest {
    private DefaultJiraAgileManager jiraAgileManager;

    @Mock
    private ApplicationLink appLink;

    @Mock
    private ApplicationLinkRequestFactory requestFactory;

    @Mock
    private ApplicationLinkRequest request;

    @Mock
    private JiraConnectorService jiraConnectorService;

    @Before
    public void setUp() {
        initMocks(this);
        when(jiraConnectorService.getJiraApplicationLink()).thenReturn(appLink);
        jiraAgileManager = new DefaultJiraAgileManager(jiraConnectorService);
    }

    @Test
    public void testQueryForRapidViews() throws Exception {
        // given
        when(appLink.createAuthenticatedRequestFactory()).thenReturn(requestFactory);
        when(requestFactory.createRequest(any(Request.MethodType.class), anyString())).thenReturn(request);
        String response = readFileToString("/json/jira-response-rapid-views.json");
        when(request.executeAndReturn(any(ReturningResponseHandler.class))).thenReturn(response);

        // when
        List<RapidView> boards = jiraAgileManager.queryForRapidViews(PROJECT_NAME);

        // then
        assertThat(boards, hasSize(2));

        assertThat(boards.get(0).getId(), is(equalTo(1)));
        assertThat(boards.get(0).getName(), is(equalTo("TP Main Board")));
        assertThat(boards.get(0).isSprintSupportEnabled(), is(equalTo(true)));

        assertThat(boards.get(1).getId(), is(equalTo(3)));
        assertThat(boards.get(1).getName(), is(equalTo("Extra Board")));
        assertThat(boards.get(1).isSprintSupportEnabled(), is(equalTo(true)));
    }

    @Test
    public void testQueryForDueDate() throws Exception {

        // given
        when(appLink.createAuthenticatedRequestFactory()).thenReturn(requestFactory);

        // TODO this is quite ugly, seems like queryForDueDate should be extracted to another service

        ApplicationLinkRequest rapidViewsRequest = mock(ApplicationLinkRequest.class);
        when(requestFactory.createRequest(any(Request.MethodType.class), contains("/rapidviews/list"))).thenReturn(rapidViewsRequest);
        when(rapidViewsRequest.executeAndReturn(any(ReturningResponseHandler.class))).thenReturn(readFileToString("/json/jira-response-rapid-views.json"));

        ApplicationLinkRequest activeSprintsRequest = mock(ApplicationLinkRequest.class);
        when(requestFactory.createRequest(any(Request.MethodType.class), contains("/integration/teamcalendars/sprint/list"))).thenReturn(activeSprintsRequest);
        when(activeSprintsRequest.executeAndReturn(any(ReturningResponseHandler.class))).thenReturn(readFileToString("/json/jira-response-active-sprints.json"));

        ApplicationLinkRequest boardSprintsRequest = mock(ApplicationLinkRequest.class);
        when(requestFactory.createRequest(any(Request.MethodType.class), contains("/sprintquery/"))).thenReturn(boardSprintsRequest);
        when(boardSprintsRequest.executeAndReturn(any(ReturningResponseHandler.class))).thenReturn(readFileToString("/json/jira-response-board-sprints.json"));

        // when
        Date dueDate = jiraAgileManager.queryForDueDate(PROJECT_NAME);

        // then
        //
        assertThat(dueDate, is(Matchers.equalTo(dateTime(2015, 6, 30, 17, 0))));
    }

    private static ZonedDateTime zonedDateTime(int year, int month, int day, int hour, int minute) {
        return ZonedDateTime.of(year, month, day, hour, minute, 0, 0, ZoneId.systemDefault());
    }

    private static Date dateTime(int year, int month, int day, int hour, int minute) {
        return Date.from(zonedDateTime(year, month, day, hour, minute).toInstant());
    }
}
