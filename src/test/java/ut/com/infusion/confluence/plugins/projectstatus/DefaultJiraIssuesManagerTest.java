package ut.com.infusion.confluence.plugins.projectstatus;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import com.infusion.confluence.plugins.projectstatus.DefaultJiraIssuesManager;
import com.infusion.confluence.plugins.projectstatus.data.*;
import com.infusion.confluence.plugins.projectstatus.services.JiraConnectorService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Wojciech Kaczmarek
 */
@SuppressWarnings("unchecked")
public class DefaultJiraIssuesManagerTest extends AbstractJiraManagerTest {

    private DefaultJiraIssuesManager jiraIssuesManager;

    @Mock
    private ApplicationLink appLink;

    @Mock
    private ApplicationLinkRequestFactory requestFactory;

    @Mock
    private ApplicationLinkRequest request;

    @Mock
    private JiraConnectorService jiraConnectorService;

    @Before
    public void setUp() {
        initMocks(this);
        jiraIssuesManager = new DefaultJiraIssuesManager(jiraConnectorService);
        when(jiraConnectorService.getJiraApplicationLink()).thenReturn(appLink);
    }

    @Test
    public void shouldParseJsonToJiraFields() throws Exception {
        // given
        when(appLink.createAuthenticatedRequestFactory()).thenReturn(requestFactory);
        when(requestFactory.createRequest(any(Request.MethodType.class), anyString())).thenReturn(request);
        String response = readFileToString("/json/jira-response-fields.json");
        when(request.executeAndReturn(any(ReturningResponseHandler.class))).thenReturn(response);

        // when
        List<JiraField> fields = jiraIssuesManager.queryForFields();

        // then
        assertThat(fields, not(empty()));
        assertThat(fields, hasSize(2));
        assertThat(fields.get(0).getName(), is(equalTo(FIELD_NAME_EPIC_LINK)));
        assertThat(fields.get(1).getName(), is(equalTo(FIELD_NAME_EPIC_NAME)));
    }

    @Test
    public void shouldParseJsonToJiraIssueTypes() throws Exception {
        // given
        when(appLink.createAuthenticatedRequestFactory()).thenReturn(requestFactory);
        when(requestFactory.createRequest(any(Request.MethodType.class), anyString())).thenReturn(request);
        String response = readFileToString("/json/jira-response-issue-types.json");
        when(request.executeAndReturn(any(ReturningResponseHandler.class))).thenReturn(response);

        // when
        List<JiraIssueType> issueTypes = jiraIssuesManager.queryForIssueTypes();

        // then
        assertThat(issueTypes, not(empty()));
        assertThat(issueTypes, hasSize(2));
        assertThat(issueTypes.get(0).getName(), is(equalTo(ISSUE_TYPE_TASK)));
        assertThat(issueTypes.get(1).getName(), is(equalTo(ISSUE_TYPE_EPIC)));
    }

    @Test
    public void shouldParseJsonToEpicIssues() throws Exception {
        // given
        when(appLink.createAuthenticatedRequestFactory()).thenReturn(requestFactory);
        when(requestFactory.createRequest(any(Request.MethodType.class), anyString())).thenReturn(request);
        String response = readFileToString("/json/jira-response-epic-issues.json");
        when(request.executeAndReturn(any(ReturningResponseHandler.class))).thenReturn(response);

        // when
        List<JiraIssue> issues = jiraIssuesManager.queryForIssues("jql=project=TP");

        // then
        assertThat(issues, not(empty()));
        assertThat(issues, hasSize(2));
        assertThat(issues.get(0).getFields(), hasEntry(EPIC_NAME_FIELD_ID, (Object)EPIC_NAME_TECHNICAL_DEBT));
        assertThat(issues.get(1).getFields(), hasEntry(EPIC_NAME_FIELD_ID, (Object)EPIC_NAME_TESTS_ENHANCEMENT));
    }

    @Test
    public void shouldReturnEmptyCollectionWhenNoIssuesFound() throws Exception {
        // given
        when(appLink.createAuthenticatedRequestFactory()).thenReturn(requestFactory);
        when(requestFactory.createRequest(any(Request.MethodType.class), anyString())).thenReturn(request);
        String response = readFileToString("/json/jira-response-no-issues.json");
        when(request.executeAndReturn(any(ReturningResponseHandler.class))).thenReturn(response);

        // when
        List<JiraIssue> issues = jiraIssuesManager.queryForIssues("jql=project=TP");

        // then
        assertThat(issues, empty());
    }

    @Test
    public void shouldParseJsonToJiraProjects() throws Exception {
        // given
        when(appLink.createAuthenticatedRequestFactory()).thenReturn(requestFactory);
        when(requestFactory.createRequest(any(Request.MethodType.class), anyString())).thenReturn(request);
        String response = readFileToString("/json/jira-response-projects.json");
        when(request.executeAndReturn(any(ReturningResponseHandler.class))).thenReturn(response);

        // when
        List<JiraProject> projects = jiraIssuesManager.queryForProjects();

        // then
        assertThat(projects, not(empty()));
        assertThat(projects, hasSize(3));
        assertThat(projects.get(1).getId(), is("10000"));
        assertThat(projects.get(1).getName(), is("Test Project"));
        assertThat(projects.get(1).getKey(), is("TP"));
        assertThat(projects.get(1).getAvatarUrls().keySet(), hasSize(4));
    }

    @Test
    public void shouldReturnListOfStatuses() throws Exception {
        // given
        when(appLink.createAuthenticatedRequestFactory()).thenReturn(requestFactory);
        when(requestFactory.createRequest(any(Request.MethodType.class), anyString())).thenReturn(request);
        String response = readFileToString("/json/jira-response-status.json");
        when(request.executeAndReturn(any(ReturningResponseHandler.class))).thenReturn(response);

        // when
        List<JiraStatus> statuses = jiraIssuesManager.queryForStatuses();

        // then
        assertThat(statuses, hasSize(9));
        assertThat(statuses.get(0).getId(), is("10002"));
        assertThat(statuses.get(0).getName(), is("New"));
        assertThat(statuses.get(0).getIconUrl(), not(isEmptyOrNullString()));
        assertThat(statuses.get(0).getStatusCategory(), notNullValue());
        assertThat(statuses.get(0).getStatusCategory().getKey(), is("new"));
        assertThat(statuses.get(0).getStatusCategory().getName(), is("To Do"));
    }

}
