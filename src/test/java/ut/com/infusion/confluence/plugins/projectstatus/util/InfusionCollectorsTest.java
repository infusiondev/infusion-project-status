package ut.com.infusion.confluence.plugins.projectstatus.util;

import com.infusion.confluence.plugins.projectstatus.util.InfusionCollectors;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class InfusionCollectorsTest {

    @Test
    public void testPartitioningBy() throws Exception {
        List<Integer> numbers = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

        List<Integer> even = new ArrayList<>();
        List<Integer> odd = new ArrayList<>();

        numbers.stream().collect(InfusionCollectors.partitioningBy(i -> i % 2 == 0, even::addAll, odd::addAll));

        assertThat(even, equalTo(Arrays.asList(0, 2, 4, 6, 8)));
        assertThat(odd, equalTo(Arrays.asList(1, 3, 5, 7, 9)));
    }
}