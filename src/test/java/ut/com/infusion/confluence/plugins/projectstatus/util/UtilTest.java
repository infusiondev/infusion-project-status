package ut.com.infusion.confluence.plugins.projectstatus.util;

import com.infusion.confluence.plugins.projectstatus.util.Util;
import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class UtilTest {

    @Test
    public void testToArrayOrNull() throws Exception {
        assertThat(Util.toArrayOrNull(null), is(nullValue()));

        String[] actual = Util.toArrayOrNull(Arrays.asList("hello", "world"));
        assertThat(Arrays.asList(actual), hasItems("hello", "world"));
    }
}
